package com.uxforms.govukdesignsystem.form.page

import com.uxforms.domain._
import com.uxforms.dsl.Form
import com.uxforms.dsl.helpers.ContainerHelper.{withReceivers, withRedirectAfterSubmissions, withRedirectBeforeRender}
import com.uxforms.dsl.helpers.VisibilityDSL.showWhenWidget
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper.noConstraints
import com.uxforms.govukdesignsystem.dsl.styles.Spacing
import com.uxforms.govukdesignsystem.dsl.widget._
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.{RedirectToPageAfterSubmission, RequireInviteCodeToAccess}
import play.api.libs.json.Json

import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

object SummaryListPage {

  val urlPath = "summary-list"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer, executionContext: ExecutionContext): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/summaryList", implicitly[ClassLoader], implicitly[Locale]))
    val visibilityChoices = Future.successful(ResourceBundleMessages.utf8("visibility", implicitly[ClassLoader], implicitly[Locale]))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess) ++
        withRedirectAfterSubmissions(RedirectToPageAfterSubmission(urlPath)) ++
        withReceivers(Seq(PopulateTestData)),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("summaryListHeading", pageMessages),
      Paragraph.body("summaryListPara", pageMessages),
      Pre.pre("summaryListWidget", pageMessages, Pre.isCode),

      SummaryList.summaryList("sList", pageMessages, Seq(
        SummaryListRow(Map("row.key.text" -> "Name", "row.value.text" -> "Sarah Philips"), Seq(Link.toPage("slRowLink", pageMessages, urlPath))),
        SummaryListRow(Map("row.key.text" -> "Date of birth", "row.value.text" -> "5 January 1978"), Seq(Link.toPage("slRowLink", pageMessages, urlPath))),
        SummaryListRow(Map("row.key.text" -> "Contact information", "row.value.html" -> "72 Guild Street<br>London<br>SE23 6FH"), Seq(Link.toPage("slRowLink", pageMessages, urlPath))),
        SummaryListRow(Map("row.key.text" -> "Contact details", "row.value.html" -> """<p class="govuk-body">07700 900457</p><p class="govuk-body">sarah.phillips@example.com</p>"""), Seq(Link.toPage("slRowLink", pageMessages, urlPath)))
      )),

      Heading.m("noActionsHeading", pageMessages, Heading.classes(Spacing.responsiveMargin(9, Spacing.direction.top))),
      Paragraph.body("noActionsPara", pageMessages),
      Pre.pre("noActionsWidget", pageMessages, Pre.isCode),
      SummaryList.summaryList("noActionsList", pageMessages, Seq(
        SummaryListRow(Map("row.key.text" -> "Name", "row.value.text" -> "Sarah Philips")),
        SummaryListRow(Map("row.key.text" -> "Date of birth", "row.value.text" -> "5 January 1978")),
        SummaryListRow(Map("row.key.text" -> "Contact information", "row.value.html" -> "72 Guild Street<br>London<br>SE23 6FH")),
        SummaryListRow(Map("row.key.text" -> "Contact details", "row.value.html" -> """<p class="govuk-body">07700 900457</p><p class="govuk-body">sarah.phillips@example.com</p>"""))
      )),


      Heading.m("dynamicListHeading", pageMessages, Heading.classes(Spacing.responsiveMargin(9, Spacing.direction.top))),
      Paragraph.body("dynamicListPara", pageMessages),
      Paragraph.body("dynamicListData", pageMessages),
      Pre.pre("dynamicListData", pageMessages, Pre.isCode),
      Paragraph.body("dynamicListMessages", pageMessages),
      Pre.pre("dynamicListMessages", pageMessages, Pre.isCode),
      Paragraph.body("dynamicListWidget", pageMessages),
      Pre.pre("dynamicListWidget", pageMessages, Pre.isCode),
      SummaryList.dynamicSummaryList("dList", pageMessages, (form, errors, requestInfo) => {
        for {
          telLabel <- pageMessages.map(_.getString("tel.label.text").getOrElse(""))
          emailLabel <- pageMessages.map(_.getString("email.label.text").getOrElse(""))
        } yield {
          val telValue = (form.data.textData \ "summaryListPage.tel").asOpt[String].getOrElse("")
          val emailValue = (form.data.textData \ "summaryListPage.email").asOpt[String].getOrElse("")
          Seq(
            SummaryListRow(Map("row.key.text" -> telLabel, "row.value.text" -> telValue)),
            SummaryListRow(Map("row.key.text" -> emailLabel, "row.value.text" -> emailValue))
          )
        }
      }),

      Heading.m("noBorderHeading", pageMessages, Heading.classes(Spacing.responsiveMargin(9, Spacing.direction.top))),
      Paragraph.body("noBorderPara", pageMessages),
      Pre.pre("noBorderWidget", pageMessages, Pre.isCode),
      SummaryList.summaryList("noBorderList", pageMessages,
        Seq(
          SummaryListRow(Map("row.key.text" -> "Name", "row.value.text" -> "Sarah Philips")),
          SummaryListRow(Map("row.key.text" -> "Date of birth", "row.value.text" -> "5 January 1978")),
          SummaryListRow(Map("row.key.text" -> "Contact information", "row.value.html" -> "72 Guild Street<br>London<br>SE23 6FH")),
          SummaryListRow(Map("row.key.text" -> "Contact details", "row.value.html" -> """<p class="govuk-body">07700 900457</p><p class="govuk-body">sarah.phillips@example.com</p>"""))
        ),
        SummaryList.noBorder
      ),


      Heading.m("visibilityHeading", pageMessages, Heading.classes(Spacing.responsiveMargin(9, Spacing.direction.top))),
      Paragraph.body("visibilityPara", pageMessages),
      Checkboxes.checkboxes("toggleListSummaryVisibility", pageMessages, noConstraints, visibilityChoices),
      SummaryList.summaryList("", pageMessages,
        Seq(
          SummaryListRow(Map("row.key.text" -> "Street", "row.value.text" -> "High Street")),
          SummaryListRow(Map("row.key.text" -> "Town", "row.value.text" -> "Market Bosingston"))
        ),
        visibility = showWhenWidget("toggleListSummaryVisibility") hasValue ("VISIBLE"))
    )

  }
}

object PopulateTestData extends DataTransformer {
  override def transform(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[DataTransformationResult] = Future.successful {
    DataTransformationResult(form.data + Json.obj("summaryListPage.tel" -> "0208 123 45678", "summaryListPage.email" -> "test@example.com"), None)
  }
}
