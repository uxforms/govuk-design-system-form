package com.uxforms.govukdesignsystem.form.page

import com.uxforms.domain._
import com.uxforms.domain.constraint.Required.required
import com.uxforms.dsl.helpers.ContainerHelper.{withReceivers, withRedirectAfterSubmissions, withRedirectBeforeRender}
import com.uxforms.dsl.helpers.VisibilityDSL.showWhenWidget
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper._
import com.uxforms.govukdesignsystem.dsl.styles.Spacing
import com.uxforms.govukdesignsystem.dsl.widget._
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.{RedirectToPageAfterSubmission, RequireInviteCodeToAccess}
import com.uxforms.govukdesignsystem.form.widget.ChangeFormDataButton
import play.api.libs.json.{JsNull, Json}

import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

object CheckboxesPage {

  val urlPath = "checkboxes"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer, executionContext: ExecutionContext): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/checkboxes", implicitly[ClassLoader], implicitly[Locale]))
    val wasteChoices = Future.successful(ResourceBundleMessages.utf8("waste", implicitly[ClassLoader], implicitly[Locale]))
    val nationalityChoices = Future.successful(ResourceBundleMessages.utf8("nationalities", implicitly[ClassLoader], implicitly[Locale]))
    val countryChoicesWithMultipleDividers = Future.successful(ResourceBundleMessages.utf8("countriesWithMultipleDividers", implicitly[ClassLoader], implicitly[Locale]))
    val contactChoices = Future.successful(ResourceBundleMessages.utf8("contact", implicitly[ClassLoader], implicitly[Locale]))
    val orgChoices = Future.successful(ResourceBundleMessages.utf8("orgs", implicitly[ClassLoader], implicitly[Locale]))
    val visibilityChoices = Future.successful(ResourceBundleMessages.utf8("visibility", implicitly[ClassLoader], implicitly[Locale]))
    val consentChoices = Future.successful(ResourceBundleMessages.utf8("consent", implicitly[ClassLoader], implicitly[Locale]))
    val euroCountryChoices = Future.successful(ResourceBundleMessages.utf8("euroCountries", implicitly[ClassLoader], implicitly[Locale]))

    val headingMarginTop = Heading.classes(Spacing.responsiveMargin(9, Spacing.direction.top))

    val randomisedCheckboxes = Checkboxes.checkboxesWithRandomisedItemOrder("randomised", pageMessages, noConstraints, orgChoices, "randomisedOrder")

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess) ++
        withReceivers(Seq(
          randomisedCheckboxes.randomiseAndPersistItemOrder)
        ) ++
        withRedirectAfterSubmissions(RedirectToPageAfterSubmission(urlPath)),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("checkboxesHeading", pageMessages),

      Paragraph.body("wastePara", pageMessages),
      Pre.pre("wasteWidget", pageMessages, Pre.isCode),
      Paragraph.body("wasteMessages", pageMessages),
      Pre.pre("wasteMessages", pageMessages, Pre.isCode),
      Paragraph.body("wasteChoices", pageMessages),
      Pre.pre("wasteChoices", pageMessages, Pre.isCode),
      Checkboxes.checkboxes("waste", pageMessages, noConstraints, wasteChoices, Checkboxes.noChoiceWidgets, Checkboxes.asPageHeading),

      Heading.m("hintHeading", pageMessages, headingMarginTop),
      Paragraph.body("hintPara", pageMessages),
      Pre.pre("hintChoices", pageMessages, Pre.isCode),
      Checkboxes.checkboxes("hints", pageMessages, noConstraints, nationalityChoices),

      Heading.m("separatorHeading", pageMessages, headingMarginTop),
      Paragraph.body("separatorPara", pageMessages),
      Pre.pre("separatorChoices", pageMessages, Pre.isCode),
      Checkboxes.checkboxes("separators", pageMessages, noConstraints, countryChoicesWithMultipleDividers),

      Heading.m("conditionalHeading", pageMessages, headingMarginTop),
      Paragraph.paragraph("conditionalPara", pageMessages),
      Pre.pre("conditionalWidget", pageMessages, Pre.isCode),
      Checkboxes.checkboxes("conditionalChecks", pageMessages, noConstraints, contactChoices, Map(
        "EMAIL" -> Input.inputText("email", pageMessages, noConstraints, Input.width20 ++ Input.formGroupClasses(Checkboxes.conditionalWidgetClass), showWhenWidget("conditionalChecks") hasValue "EMAIL"),
        "PHONE" -> Input.inputText("phone", pageMessages, noConstraints, Input.width10 ++ Input.formGroupClasses(Checkboxes.conditionalWidgetClass), showWhenWidget("conditionalChecks") hasValue "PHONE"),
        "SMS" -> Input.inputText("sms", pageMessages, noConstraints, Input.width10 ++ Input.formGroupClasses(Checkboxes.conditionalWidgetClass), showWhenWidget("conditionalChecks") hasValue "SMS")
      )),

      Heading.m("noneHeading", pageMessages, headingMarginTop),
      Paragraph.body("noneIntro", pageMessages),
      Paragraph.body("noneChoices", pageMessages),
      Pre.pre("noneChoices", pageMessages, Pre.isCode),
      Checkboxes.checkboxes("noneChecks", pageMessages, noConstraints, euroCountryChoices),

      Heading.m("smallerHeading", pageMessages, headingMarginTop),
      Pre.pre("smallerWidget", pageMessages, Pre.isCode),
      Checkboxes.checkboxes("smallerChecks", pageMessages, noConstraints, orgChoices, Checkboxes.noChoiceWidgets, Checkboxes.small),

      Heading.m("errorHeading", pageMessages, headingMarginTop),
      Paragraph.body("errorPara", pageMessages),
      Paragraph.body("errorWidget", pageMessages),
      Pre.pre("errorWidget", pageMessages, Pre.isCode),
      Paragraph.body("errorMessages", pageMessages),
      Pre.pre("errorMessages", pageMessages, Pre.isCode),
      Checkboxes.checkboxes("errorChecks", pageMessages, required()(Messages.empty()), nationalityChoices),
      Button.button("validate", pageMessages),

      Heading.m("randomised", pageMessages, headingMarginTop),
      Paragraph.body("randomised", pageMessages),
      Paragraph.body("randomisedHow", pageMessages),
      Pre.pre("randomisedHow", pageMessages, Pre.isCode),
      Paragraph.body("randomisedReceive", pageMessages),
      Pre.pre("randomisedReceive", pageMessages, Pre.isCode),
      Paragraph.body("randomisedFixed", pageMessages),
      Pre.pre("randomisedFixed", pageMessages, Pre.isCode),
      randomisedCheckboxes,
      new ChangeFormDataButton(
        "reRandomise",
        (_, existingData) => existingData + Json.obj(randomisedCheckboxes.randomisedItemOrderKey -> JsNull),
        Button.button("reRandomise", pageMessages, Button.secondary)
      ),

      Heading.m("checkboxesVisibility", pageMessages, headingMarginTop),
      Paragraph.body("visibilityPara", pageMessages),
      Checkboxes.checkboxes("toggleCheckboxesVisibility", pageMessages, noConstraints, visibilityChoices, Checkboxes.noChoiceWidgets, NoExtraRenderArgs),
      Checkboxes.checkboxes("checkboxesVisibility", pageMessages, noConstraints, consentChoices, Checkboxes.noChoiceWidgets, NoExtraRenderArgs, showWhenWidget("toggleCheckboxesVisibility") hasValue "VISIBLE"),

      new ChangeFormDataButton(
        "reset",
        (_, existingData) => existingData + Json.obj(
          "randomised" -> JsNull,
          "waste" -> JsNull,
          "hints" -> JsNull,
          "separators" -> JsNull,
          "conditionalChecks" -> JsNull,
          "noneChecks" -> JsNull,
          "smallerChecks" -> JsNull,
          "errorChecks" -> JsNull,
          "toggleCheckboxesVisibility" -> JsNull,
          "checkboxesVisibility" -> JsNull
        ),
        Button.button("reset", pageMessages, Button.secondary)
      )
    )
  }

}




