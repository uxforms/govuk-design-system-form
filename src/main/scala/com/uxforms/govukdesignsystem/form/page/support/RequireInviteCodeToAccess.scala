package com.uxforms.govukdesignsystem.form.page.support

import com.uxforms.domain.env.EnvironmentHelper
import com.uxforms.domain.{FormData, RequestInfo}
import com.uxforms.dsl.containers.{RedirectBeforeRender, RedirectTo, RedirectToForm}
import com.uxforms.govukdesignsystem.form.section.Welcome

import scala.concurrent.{ExecutionContext, Future}

object RequireInviteCodeToAccess extends RedirectBeforeRender {

  private val env = new EnvironmentHelper().env

  override def redirect(data: FormData, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Option[Future[RedirectTo]] = {
    if (env == "local" || Welcome.hasInviteCode(data)) {
      None
    } else {
      Some(Future.successful(RedirectToForm))
    }

  }
}
