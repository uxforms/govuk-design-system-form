package com.uxforms.govukdesignsystem.form.page

import com.uxforms.domain._
import com.uxforms.domain.constraint.Required.required
import com.uxforms.dsl.helpers.ContainerHelper.{withReceivers, withRedirectAfterSubmissions, withRedirectBeforeRender}
import com.uxforms.dsl.helpers.VisibilityDSL.showWhenWidget
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper._
import com.uxforms.govukdesignsystem.dsl.styles.Spacing
import com.uxforms.govukdesignsystem.dsl.widget._
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.{ClearWidgetValues, RedirectToPageAfterSubmission, RequireInviteCodeToAccess}
import com.uxforms.govukdesignsystem.form.widget.ChangeFormDataButton
import play.api.libs.json.{JsNull, Json}

import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

object RadiosPage {

  val urlPath = "radios"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer, executionContext: ExecutionContext): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/radios", implicitly[ClassLoader], implicitly[Locale]))
    val yesNoChoices = Future.successful(ResourceBundleMessages.utf8("yesNo", implicitly[ClassLoader], implicitly[Locale]))
    val countryChoices = Future.successful(ResourceBundleMessages.utf8("countries", implicitly[ClassLoader], implicitly[Locale]))
    val countryChoicesWithDivider = Future.successful(ResourceBundleMessages.utf8("countriesWithDivider", implicitly[ClassLoader], implicitly[Locale]))
    val countryChoicesWithMultipleDividers = Future.successful(ResourceBundleMessages.utf8("countriesWithMultipleDividers", implicitly[ClassLoader], implicitly[Locale]))
    val signInChoices = Future.successful(ResourceBundleMessages.utf8("signIn", implicitly[ClassLoader], implicitly[Locale]))
    val contactChoices = Future.successful(ResourceBundleMessages.utf8("contact", implicitly[ClassLoader], implicitly[Locale]))
    val frequencyChoices = Future.successful(ResourceBundleMessages.utf8("frequency", implicitly[ClassLoader], implicitly[Locale]))
    val visibilityChoices = Future.successful(ResourceBundleMessages.utf8("visibility", implicitly[ClassLoader], implicitly[Locale]))

    val headingMarginTop = Heading.classes(Spacing.responsiveMargin(9, Spacing.direction.top))

    val randomisedRadios = Radios.radiosWithRandomisedItemOrder("randomised", pageMessages, noConstraints, countryChoices, "randomisedRadiosOrder")

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess) ++
        withReceivers(Seq(randomisedRadios.randomiseAndPersistItemOrder)) ++
        withRedirectAfterSubmissions(RedirectToPageAfterSubmission(urlPath)),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("radiosHeading", pageMessages),

      Paragraph.body("radiosPara", pageMessages),
      Pre.pre("radiosParaWidget", pageMessages, Pre.isCode),
      Paragraph.body("radiosParaChoices", pageMessages),
      Pre.pre("radiosParaChoices", pageMessages, Pre.isCode),
      Paragraph.body("radiosParaMessages", pageMessages),
      Pre.pre("radiosParaMessages", pageMessages, Pre.isCode),
      Radios.radios("yesNo", pageMessages, noConstraints, yesNoChoices, Radios.noChoiceWidgets, Radios.inline ++ Radios.asPageHeading),

      Heading.m("radiosStackedHeading", pageMessages, headingMarginTop),
      Paragraph.body("radiosStacked", pageMessages),
      Pre.pre("radiosStacked", pageMessages, Pre.isCode),
      Radios.radios("stacked", pageMessages, noConstraints, countryChoices),

      Heading.m("radiosWithHints", pageMessages, headingMarginTop),
      Paragraph.body("radiosWithHintsPara", pageMessages),
      Pre.pre("radiosWithHintsWidget", pageMessages, Pre.isCode),
      Paragraph.body("radiosWithHintsChoices", pageMessages),
      Pre.pre("radiosWithHintsChoices", pageMessages, Pre.isCode),
      Radios.radios("withHints", pageMessages, noConstraints, signInChoices),

      Heading.m("radiosWithDivider", pageMessages, headingMarginTop),
      Paragraph.body("radiosWithDividerPara", pageMessages),
      Pre.pre("radiosWithDividerChoices", pageMessages, Pre.isCode),
      Radios.radios("withDivider", pageMessages, noConstraints, countryChoicesWithDivider),
      Paragraph.body("radiosWithMultipleDividersPara", pageMessages),
      Pre.pre("radiosWithMultipleDividersChoices", pageMessages, Pre.isCode),
      Radios.radios("withMultipleDividers", pageMessages, noConstraints, countryChoicesWithMultipleDividers),

      Heading.m("radiosWithConditionals", pageMessages, headingMarginTop),
      Paragraph.body("radiosWithConditionalsPara", pageMessages),
      Pre.pre("radiosWithConditionalsWidget", pageMessages),
      Radios.radios("withConditionals", pageMessages, noConstraints, contactChoices, Map(
        "EMAIL" -> Input.inputText("email", pageMessages, noConstraints, Input.width20 ++ Input.formGroupClasses(Radios.conditionalWidgetClass), showWhenWidget("withConditionals") hasValue "EMAIL"),
        "PHONE" -> Input.inputText("phone", pageMessages, noConstraints, Input.width10 ++ Input.formGroupClasses(Radios.conditionalWidgetClass), showWhenWidget("withConditionals") hasValue "PHONE"),
        "SMS" -> Input.inputText("sms", pageMessages, noConstraints, Input.width10 ++ Input.formGroupClasses(Radios.conditionalWidgetClass), showWhenWidget("withConditionals") hasValue "SMS")
      )),

      Heading.m("radiosSmall", pageMessages, headingMarginTop),
      Pre.pre("radiosSmallPara", pageMessages, Pre.isCode),
      Radios.radios("smaller", pageMessages, noConstraints, frequencyChoices, extraTemplateRenderArgs = Radios.small),

      Heading.m("radiosValidation", pageMessages, headingMarginTop),
      Paragraph.body("radiosValidationPara", pageMessages),
      Pre.pre("radiosValidationWidget", pageMessages, Pre.isCode),
      Paragraph.body("radiosValidationMessages", pageMessages),
      Pre.pre("radiosValidationMessages", pageMessages, Pre.isCode),
      Radios.radios("radiosValid", pageMessages, required()(Messages.empty()), yesNoChoices),
      Button.button("validate", pageMessages),

      Heading.m("randomised", pageMessages, headingMarginTop),
      Paragraph.body("randomised", pageMessages),
      Paragraph.body("randomisedHow", pageMessages),
      Pre.pre("randomisedHow", pageMessages, Pre.isCode),
      Paragraph.body("randomisedReceive", pageMessages),
      Pre.pre("randomisedReceive", pageMessages, Pre.isCode),
      Paragraph.body("randomisedFixed", pageMessages),
      Pre.pre("randomisedFixed", pageMessages, Pre.isCode),
      randomisedRadios,
      new ChangeFormDataButton(
        "reRandomise",
        (_, existingData) => existingData + Json.obj(randomisedRadios.randomisedItemOrderKey -> JsNull),
        Button.button("reRandomise", pageMessages, Button.secondary)
      ),


      Heading.m("radiosVisibility", pageMessages, headingMarginTop),
      Paragraph.body("visibilityPara", pageMessages),
      Checkboxes.checkboxes("toggleRadiosVisibility", pageMessages, noConstraints, visibilityChoices),
      Radios.radios("radiosVisibility", pageMessages, noConstraints, yesNoChoices, Radios.noChoiceWidgets, NoExtraRenderArgs, showWhenWidget("toggleRadiosVisibility") hasValue ("VISIBLE")),

      new ChangeFormDataButton("resetRadioData", (_,existingData) => existingData + Json.obj(
        "yesNo" -> JsNull,
        "stacked" -> JsNull,
        "withHints" -> JsNull,
        "withDivider" -> JsNull,
        "withMultipleDividers" -> JsNull,
        "withConditionals" -> JsNull,
        "smaller" -> JsNull,
        "radiosValid" -> JsNull,
        "radiosVisibility" -> JsNull
      ), Button.button("resetRadioData", pageMessages, Button.secondary))
    )
  }

}




