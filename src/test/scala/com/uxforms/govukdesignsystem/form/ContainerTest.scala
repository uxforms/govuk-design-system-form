package com.uxforms.govukdesignsystem.form

import com.uxforms.domain.FormDefinitionFactory
import com.uxforms.govukdesignsystem.form.build.GOVUKDesignSystemFormDefinitionBuildInfo
import com.uxforms.test.OsgiContainer
import javax.inject.Inject
import org.hamcrest.CoreMatchers._
import org.hamcrest.MatcherAssert._
import org.junit.Test
import org.junit.runner.RunWith
import org.ops4j.pax.exam.CoreOptions._
import org.ops4j.pax.exam.Option
import org.ops4j.pax.exam.junit.PaxExam
import org.ops4j.pax.exam.spi.reactors.{ExamReactorStrategy, PerSuite}
import org.ops4j.pax.exam.util.{Filter, PathUtils}

@RunWith(classOf[PaxExam])
@ExamReactorStrategy(Array(classOf[PerSuite]))
class ContainerTest extends OsgiContainer {

  override val projectArtifact: Option = composite(
    bundle(GOVUKDesignSystemFormDefinitionBuildInfo.artifact.toURI.toURL.toExternalForm),
    systemProperty("logback.configurationFile").value(s"file:${PathUtils.getBaseDir}/src/test/resources/logback.xml")
  )


  @Inject
  @Filter("Bundle-SymbolicName=govuk-design-system-form")
  var formDefFactory: FormDefinitionFactory = _

  @Test
  def containerShouldStartUpSuccessfully(): Unit = {
    assertThat(context, is(notNullValue()))
  }

  @Test
  def formDefinitionShouldBeAvailableInOsgiContext(): Unit = {
    assertThat(formDefFactory, is(notNullValue()))
  }

}