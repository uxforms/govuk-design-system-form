package com.uxforms.govukdesignsystem.form.page

import com.uxforms.domain.{Messages, ResourceBundleMessages}
import com.uxforms.dsl.helpers.ContainerHelper.withRedirectBeforeRender
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.widget.{BackLink, Heading, InsetText, Paragraph, Pre, SectionBreak, Tag}
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.RequireInviteCodeToAccess

import java.util.Locale
import scala.concurrent.Future

object TagPage {

  val urlPath = "tag"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/tag", implicitly[ClassLoader], implicitly[Locale]))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("tagHeading", pageMessages),
      Paragraph.body("tagIntro", pageMessages),

      Paragraph.body("t", pageMessages),
      Pre.pre("t", pageMessages, Pre.isCode),
      Tag.tag("t", pageMessages),

      SectionBreak.xl("preColourHeading"),

      Heading.m("tagColours", pageMessages),

      Paragraph.body("tagColourMsgs", pageMessages),
      Pre.pre("tagColourMsgs", pageMessages, Pre.isCode),

      SectionBreak.xl("preColourExamples"),
      Pre.pre("tagInactive", pageMessages, Pre.isCode),
      Tag.tag("tagInactive", pageMessages, Tag.grey),

      Pre.pre("tagNew", pageMessages, Pre.isCode),
      Tag.tag("tagNew", pageMessages, Tag.green),

      Pre.pre("tagActive", pageMessages, Pre.isCode),
      Tag.tag("tagActive", pageMessages, Tag.turquoise),

      Pre.pre("tagPending", pageMessages, Pre.isCode),
      Tag.tag("tagPending", pageMessages, Tag.blue),

      Pre.pre("tagReceived", pageMessages, Pre.isCode),
      Tag.tag("tagReceived", pageMessages, Tag.purple),

      Pre.pre("tagSent", pageMessages, Pre.isCode),
      Tag.tag("tagSent", pageMessages, Tag.pink),

      Pre.pre("tagRejected", pageMessages, Pre.isCode),
      Tag.tag("tagRejected", pageMessages, Tag.red),

      Pre.pre("tagDeclined", pageMessages, Pre.isCode),
      Tag.tag("tagDeclined", pageMessages, Tag.orange),

      Pre.pre("tagDelayed", pageMessages, Pre.isCode),
      Tag.tag("tagDelayed", pageMessages, Tag.yellow)
    )

  }
}
