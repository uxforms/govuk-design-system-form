package com.uxforms.govukdesignsystem.form.page

import com.uxforms.domain.ExtraTemplateRenderArgsOps._
import com.uxforms.domain.{Messages, ResourceBundleMessages}
import com.uxforms.dsl.helpers.ContainerHelper.withRedirectBeforeRender
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.widget.MessageContent.messageContent
import com.uxforms.govukdesignsystem.dsl.widget.Tag.tag
import com.uxforms.govukdesignsystem.dsl.widget._
import com.uxforms.govukdesignsystem.dsl.widget.templateargs.TemplateArgManipulator
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.RequireInviteCodeToAccess
import TemplateArgManipulator.mergeExtraTemplateRenderArgs

import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

object TaskListPage {

  val urlPath = "task-list"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, executionContext: ExecutionContext, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/taskList", implicitly[ClassLoader], implicitly[Locale]))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("taskListHeading", pageMessages),
      Paragraph.body("taskListIntro", pageMessages),

      Paragraph.body("taskListSimple", pageMessages),
      Pre.pre("staticExample", pageMessages, Pre.isCode),
      Paragraph.body("staticExampleMessages", pageMessages),
      Pre.pre("staticExampleMessages", pageMessages, Pre.isCode),
      TaskList.taskList("tl", pageMessages, Seq(
        TaskListSection(messageContent(pageMessages, "tl.firstSection.heading"), Seq(
          TaskListItem(Link.toExternalUrl("tl.firstItem", pageMessages, "#"), tag("completeTag", pageMessages, TaskList.rightAlignTag)),
          TaskListItem(Link.toExternalUrl("tl.secondItem", pageMessages, "#"), tag("inProgressTag", pageMessages, mergeExtraTemplateRenderArgs(TaskList.rightAlignTag, Tag.blue)))
        )),
        TaskListSection(messageContent(pageMessages, "tl.secondSection.heading"), Seq(
          TaskListItem(Link.toExternalUrl("tl.thirdItem", pageMessages, "#"), tag("notStartedTag", pageMessages, mergeExtraTemplateRenderArgs(TaskList.rightAlignTag, Tag.grey))),
          TaskListItem(messageContent(pageMessages, "tl.fourthItem"), tag("cannotStartTag", pageMessages, mergeExtraTemplateRenderArgs(TaskList.rightAlignTag, Tag.grey)))
        ))
      )),

      Heading.m("dynamicExample", pageMessages),

      Paragraph.body("dynamicExample", pageMessages),
      Paragraph.body("dynamicHowTo", pageMessages),
      Pre.pre("dynamicHowTo", pageMessages, Pre.isCode),
      Paragraph.body("dynamicPageOrder", pageMessages),
      Paragraph.body("dynamicPageDefinition", pageMessages),
      Pre.pre("dynamicPageImplementation", pageMessages, Pre.isCode)
    )
  }
}
