package com.uxforms.govukdesignsystem.form.page

import com.uxforms.domain.{Messages, ResourceBundleMessages}
import com.uxforms.dsl.helpers.ContainerHelper.withRedirectBeforeRender
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.styles.Spacing
import com.uxforms.govukdesignsystem.dsl.widget.{BackLink, Heading, Paragraph, Table}
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.RequireInviteCodeToAccess

import java.util.Locale
import scala.concurrent.Future

object SpacingPage {

  val urlPath = "spacing"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/spacing", implicitly[ClassLoader], implicitly[Locale]))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("spacingHeading", pageMessages),

      Paragraph.body("spacingIntro", pageMessages),

      Heading.m("responsive", pageMessages, Heading.classes(Spacing.responsiveMargin(9, Spacing.direction.top))),

      Table.table("margin", pageMessages, Seq(
        Map("text" -> "code"),
        Map("text" -> "generated class")
      ),
        (0 to 9).map(i => Seq(
          Map("text" -> s"Spacing.responsiveMargin(${i})"),
          Map("text" -> s"govuk-!-margin-${i}")
        ))
      ),

      Table.table("padding", pageMessages, Seq(
        Map("text" -> "code"),
        Map("text" -> "generated class")
      ),
        (0 to 9).map(i => Seq(
          Map("text" -> s"Spacing.responsivePadding(${i})"),
          Map("text" -> s"govuk-!-padding-${i}")
        ))
      ),

      Heading.m("static", pageMessages, Heading.classes(Spacing.responsiveMargin(9, Spacing.direction.top))),

      Table.table("margin", pageMessages, Seq(
        Map("text" -> "code"),
        Map("text" -> "generated class")
      ),
        (0 to 9).map(i => Seq(
          Map("text" -> s"Spacing.staticMargin(${i})"),
          Map("text" -> s"govuk-!-static-margin-${i}")
        ))
      ),

      Table.table("padding", pageMessages, Seq(
        Map("text" -> "code"),
        Map("text" -> "generated class")
      ),
        (0 to 9).map(i => Seq(
          Map("text" -> s"Spacing.staticPadding(${i})"),
          Map("text" -> s"govuk-!-static-padding-${i}")
        ))
      ),

      Heading.m("direction", pageMessages, Heading.classes(Spacing.responsiveMargin(9, Spacing.direction.top))),
      Paragraph.body("direction", pageMessages)
    )
  }


}
