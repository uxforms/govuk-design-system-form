package com.uxforms.govukdesignsystem.form

import com.uxforms.domain.{FormDefinition, FormDefinitionFactory, ResourceBundleMessages}
import com.uxforms.dsl.helpers.FormDefinitionHelper._
import com.uxforms.govukdesignsystem.dsl.spullara.SpullaraTemplateRenderer
import com.uxforms.govukdesignsystem.dsl.{Completed, FormDefinitionBuilder, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.build.GOVUKDesignSystemFormDefinitionBuildInfo
import com.uxforms.govukdesignsystem.form.page._
import com.uxforms.govukdesignsystem.form.section.{Browse, Welcome}

import java.util.Locale
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

object GOVUKDesignSystemFormDefinitionFactory extends FormDefinitionFactory {

  /**
   * This is which locales are supported by your form, in order of preference.
   * So if your user doesn't explicitly state which locale they want the form in, they
   * will get the first in this sequence.
   */
  override val supportedLocales: Seq[Locale] = Seq(Locale.UK)

  /**
   * Makes my classLoader available implicitly so that message bundles can be referenced
   * easily.
   */
  implicit val localClassLoader: ClassLoader = getClass.getClassLoader

  implicit val templateEngine: TemplateRenderer = new SpullaraTemplateRenderer(GOVUKDesignSystemFormDefinitionBuildInfo.themeName, Some("assets/templates"))


  /**
   * Factory method for instantiating your form definition.
   */
  override def formDefinition(requestedLocale: Locale)(implicit executionContext: ExecutionContext): FormDefinition = {

    /**
     * Resolves the locale requested by the user from a combination of their HTTP headers,
     * explicitly requested locale (i.e. in the URL), and those supported by this form definition.
     */
    implicit val locale: Locale = resolveRequestedLocale(requestedLocale)

    implicit val formLevelMessages: ResourceBundleMessages = ResourceBundleMessages.utf8("formMessages", implicitly[ClassLoader], implicitly[Locale])
    implicit val futureFormLevelMessages: Future[ResourceBundleMessages] = Future.successful(formLevelMessages)

    /**
     * This is where the questions for your form are defined.
     */
    FormDefinitionBuilder.formDefinition(

      GOVUKDesignSystemFormDefinitionBuildInfo.name,

      formLevelMessages,

      locale,

      Seq(
        BackLinkPage(),
        ButtonPage(),
        CharacterCountPage(),
        CheckboxesPage(),
        DatePage(),
        DetailsPage(),
        FieldsetPage(),
        FileUploadPage(),
        InputPage(),
        InsetTextPage(),
        PanelPage(),
        PrePage(),
        RadiosPage(),
        SummaryListPage(),
        TabsPage(),
        TablePage(),
        SelectPage(),
        SpacingPage(),
        TagPage(),
        TaskListPage(),
        TextAreaPage(),
        TypographyPage(),
        WarningTextPage(),

        ConfirmationPage(),
        PageNotFoundPage(),
        ProblemWithServicePage(),
        QuestionPage(),
        ServiceUnavailablePage()
      ),

      noResumePages,

      noTransformers,

      Duration(GOVUKDesignSystemFormDefinitionBuildInfo.retentionPeriod),

      Completed.noCompletedSection(),

      Welcome(),
      Browse()

    )
  }
}
