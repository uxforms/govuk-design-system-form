package com.uxforms.govukdesignsystem.form.page.support

import com.uxforms.domain.{DataTransformationResult, DataTransformer, RequestInfo}
import com.uxforms.dsl.Form

import scala.concurrent.{ExecutionContext, Future}

object ClearWidgetValues extends DataTransformer {

  val requestParameterName = "clearWidgetValues"

  override def transform(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[DataTransformationResult] = {

    val data = requestInfo.queryData.get(requestParameterName)
      .map(_.foldLeft(form.data)((data, widgetName) => data - widgetName))
      .getOrElse(form.data)

    Future.successful(DataTransformationResult(data, None))
  }
}
