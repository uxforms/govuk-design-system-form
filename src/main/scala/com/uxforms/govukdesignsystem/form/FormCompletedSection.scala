package com.uxforms.govukdesignsystem.form

import com.uxforms.domain.widget.Widget
import com.uxforms.domain.{EmptyMessages, Messages, NoExtraRenderArgs, RequestInfo, ValidationErrors}
import com.uxforms.dsl.Form
import com.uxforms.dsl.containers.mustache.CompletedSection

import scala.concurrent.{ExecutionContext, Future}

object FormCompletedSection  {

  def apply(): CompletedSection[EmptyMessages] =
    new CompletedSection(Messages.empty, Seq.empty, "", NoExtraRenderArgs, "", null) {
      override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo, runtimeWidgets: Widget*)(implicit executionContext: ExecutionContext): Future[String] =
        Future.successful("")
    }



}
