
import scala.concurrent.duration._

val basePackage = "com.uxforms.govukdesignsystem.form"

enablePlugins(SbtOsgi, FormDefinitionPlugin, BuildInfoPlugin)

val govukDesignSystemVersion = "1.6.5"

buildInfoPackage := s"$basePackage.build"

buildInfoKeys := Seq[BuildInfoKey](
  name,
  "artifact" -> (Compile / packageBin / artifactPath).value,
  themeName,
  retentionPeriod,
  "govukDesignSystemVersion" -> govukDesignSystemVersion
)

buildInfoObject := "GOVUKDesignSystemFormDefinitionBuildInfo"

organization := "com.uxforms"

name := "govuk-design-system-form"

themeName := "govuk-design-system-theme"

retentionPeriod := 1.hour

formDefinitionClass := s"$basePackage.GOVUKDesignSystemFormDefinitionFactory"

scalaVersion := "2.11.7"

test := ((Test / test) dependsOn OsgiKeys.bundle).value

resolvers += Resolver.mavenLocal

libraryDependencies ++= {
  Seq(
    "com.uxforms" %% "uxforms-dsl" % "15.27.1",
    "com.uxforms" %% "govuk-design-system-dsl" % govukDesignSystemVersion,
    "com.uxforms" %% "test" % "15.27.0" % Test,
    "org.scalatest" %% "scalatest" % "2.2.4" % Test
  )
}

publishTo := Some("publish-uxforms-public" at "s3://artifacts-public.uxforms.net")

// Include [skip ci] in commit messages to prevent the release commit triggering a build
releaseTagComment        := s"[skip ci] Releasing ${(ThisBuild / version).value}"
releaseCommitMessage     := s"[skip ci] Setting version to ${(ThisBuild / version).value}"
releaseNextCommitMessage := s"[skip ci] Setting version to ${(ThisBuild / version).value}"

// We must export our form definition factory and activator's package to the OSGi context
// so that UX Forms can load and execute this form definition.
OsgiKeys.exportPackage := Seq(s"$basePackage.*")

// Register our jar as a FormDefinition in the OSGi context
OsgiKeys.bundleActivator := Some(s"$basePackage.Activator")

// Prevent our own form classes from also being imported
OsgiKeys.importPackage := Seq(
  s"!$basePackage.*",
  "!sun.misc.*"
) ++ OsgiKeys.importPackage.value

// Don't assume package names are the same as the project name
OsgiKeys.privatePackage := OsgiKeys.privatePackage.value.filterNot(_ == s"${OsgiKeys.bundleSymbolicName.value}.*") ++ Seq(
  "com.github.mustachejava.*",
  "com.github.blemale.scaffeine.*",
  "com.github.benmanes.caffeine.*",
  "com.google.errorprone.*",
  "org.checkerframework.*",
  "scala.compat.java8.*",
  "scala.concurrent.java8"
)


// Add the *names* of jarfiles you want to be embedded within this form's jar.
// Typically used to include third-party libs (and any names of their transitive dependent jars).
// Add your jar name prefixes to embeddedJarNames, and the filter and embeddedJars property below will do the rest for you.
val embeddedJarNames = Seq(
  "govuk-design-system-dsl"
)
val embeddedJarFilter: (File) => Boolean = f => embeddedJarNames.exists(jarName => f.getName.startsWith(jarName))
OsgiKeys.embeddedJars := (Compile / Keys.externalDependencyClasspath).value map(_.data) filter embeddedJarFilter

// Force the build to use Java 8
javacOptions ++= Seq("-source", "1.8", "-target", "1.8")
scalacOptions := Seq("-target:jvm-1.8")
OsgiKeys.requireCapability := "osgi.ee;filter:=\"(&(osgi.ee=JavaSE)(version=1.8))\""
