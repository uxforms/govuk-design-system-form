package com.uxforms.govukdesignsystem.form.page

import com.uxforms.domain.constraint.Required.required
import com.uxforms.domain._
import com.uxforms.dsl.Form
import com.uxforms.dsl.helpers.All.withReceivers
import com.uxforms.dsl.helpers.ContainerHelper.{withRedirectAfterSubmissions, withRedirectBeforeRender}
import com.uxforms.dsl.helpers.VisibilityDSL.showWhenWidget
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper._
import com.uxforms.govukdesignsystem.dsl.styles.Spacing
import com.uxforms.govukdesignsystem.dsl.styles.Spacing.direction
import com.uxforms.govukdesignsystem.dsl.template.Label
import com.uxforms.govukdesignsystem.dsl.widget._
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.{RedirectToPageAfterSubmission, RequireInviteCodeToAccess}
import play.api.libs.json.Json

import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

object CharacterCountPage {

  val urlPath = "characterCount"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer, ec: ExecutionContext): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/characterCount", implicitly[ClassLoader], implicitly[Locale]))
    val visibilityChoices = Future.successful(ResourceBundleMessages.utf8("visibility", implicitly[ClassLoader], implicitly[Locale]))
    implicit val validationMessages: EmptyMessages = Messages.empty()

    val topMargin = Heading.classes(Spacing.responsiveMargin(9, direction.top))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess) ++
        withRedirectAfterSubmissions(RedirectToPageAfterSubmission(urlPath)) ++
        withReceivers(Seq(populateTestData)),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("characterCountHeading", pageMessages),

      Paragraph.body("charCount", pageMessages),
      Pre.pre("charCount", pageMessages, Pre.isCode),
      Paragraph.body("charCountMsgs", pageMessages),
      Pre.pre("charCountMsgs", pageMessages, Pre.isCode),
      CharacterCount.characterCount("moreDetail", pageMessages, noConstraints, 200, Label.asPageHeading),

      Heading.m("moreThanOne", pageMessages, topMargin),
      Paragraph.body("moreThanOne", pageMessages),
      Pre.pre("moreThanOne", pageMessages, Pre.isCode),
      CharacterCount.characterCount("moreThanOne", pageMessages, noConstraints, 200),

      Heading.m("maxWords", pageMessages, topMargin),
      Pre.pre("maxWords", pageMessages, Pre.isCode),
      CharacterCount.wordCount("maxWords", pageMessages, noConstraints, 150),

      Heading.m("narrowLimits", pageMessages, topMargin),
      Paragraph.body("narrowLimits", pageMessages),
      Pre.pre("narrowLimits", pageMessages),
      CharacterCount.characterCount("narrowLimits", pageMessages, noConstraints, 112, CharacterCount.threshold(75) ++
        SimpleExtraTemplateRenderArgs("textArea.value" -> "Type another letter into this field after this message to see the threshold feature")),

      Heading.m("errorMessages", pageMessages, topMargin),
      Paragraph.body("errorMessages", pageMessages),
      Paragraph.body("errorMessagesTrigger", pageMessages),
      Pre.pre("errorMessagesMsgs", pageMessages, Pre.isCode),
      Pre.pre("errorMessages", pageMessages, Pre.isCode),
      CharacterCount.characterCount("errorMessages", pageMessages, noConstraints, 350),
      Button.button("validate", pageMessages),

      Heading.m("visibility", pageMessages, topMargin),
      Paragraph.body("visibility", pageMessages),
      Pre.pre("visibility", pageMessages, Pre.isCode),
      Checkboxes.checkboxes("toggleButtonVisibility", pageMessages, noConstraints, visibilityChoices),
      CharacterCount.characterCount("visibility", pageMessages, noConstraints, 200, NoExtraRenderArgs, showWhenWidget("toggleButtonVisibility") hasValue ("VISIBLE"))
    )
  }

  private val populateTestData = new DataTransformer {
    override def transform(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[DataTransformationResult] = {
      val tooLongText = "A content designer works on the end-to-end journey of a service to help users complete their goal and government deliver a policy intent. Their work may involve the creation of, or change to, a transaction, product or single piece of content that stretches across digital and offline channels. They make sure appropriate content is shown to a user in the right place and in the best format."
      Future.successful(DataTransformationResult(form.data + Json.obj("errorMessages" -> tooLongText), None))
    }
  }
}
