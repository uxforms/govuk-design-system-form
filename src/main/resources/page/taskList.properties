taskListHeading.heading.heading=Task List
taskListHeading.heading.caption=Patterns


taskListIntro.paragraph.html=This is a task list. As of the time of writing this pattern is still deemed 'Experimental' so some of its \
  implementation is missing from GOV.UK's Design System. This implementation has been informed by \
  <a href="https://github.com/alphagov/govuk-prototype-kit" target="_blank" rel="noopener">https://github.com/alphagov/govuk-prototype-kit</a>.

taskListSimple.paragraph.text=For the most simple task list, we can build the entries statically in our Section declaration:
staticExample.pre.text=TaskList.taskList("tl", pageMessages, Seq(\n  \
  TaskListSection(messageContent(pageMessages, "tl.firstSection.heading"), Seq(\n    \
    TaskListItem(\n      \
      Link.toExternalUrl("tl.firstItem", pageMessages, "#"),\n      \
      tag("completeTag", pageMessages, TaskList.rightAlignTag)),\n    \
    TaskListItem(\n      \
      Link.toExternalUrl("tl.secondItem", pageMessages, "#"),\n      \
      tag("inProgressTag", pageMessages, TaskList.rightAlignTag.merge(Tag.blue)))\n  \
  )),\n  \
  TaskListSection(messageContent(pageMessages, "tl.secondSection.heading"), Seq(\n    \
    TaskListItem(\n      \
      Link.toExternalUrl("tl.thirdItem", pageMessages, "#"),\n      \
      tag("notStartedTag", pageMessages, TaskList.rightAlignTag.merge(Tag.grey))),\n    \
    TaskListItem(\n      \
      messageContent(pageMessages, "tl.fourthItem"),\n      \
      tag("cannotStartTag", pageMessages, TaskList.rightAlignTag.merge(Tag.grey)))\n  \
  ))\n\
 ))
staticExampleMessages.paragraph.text=And then with the following entries in its Messages:
staticExampleMessages.pre.text=tl.firstSection.heading=Check before you start\n\
  tl.firstItem.link.text=Check eligibility\n\
  tl.secondItem.link.text=Read declaration\n\
  \n\
  tl.secondSection.heading=Prepare application\n\
  tl.thirdItem.link.text=Company information\n\
  tl.fourthItem=Your contact details\n\
  \n\
  completeTag.tag.text=Completed\n\
  inProgressTag.tag.text=In progress\n\
  notStartedTag.tag.text=Not started\n\
  cannotStartTag.tag.text=Cannot start yet

tl.firstSection.heading=Check before you start
tl.firstItem.link.text=Check eligibility
tl.secondItem.link.text=Read declaration

tl.secondSection.heading=Prepare application
tl.thirdItem.link.text=Company information
tl.fourthItem=Your contact details

completeTag.tag.text=Completed
inProgressTag.tag.text=In progress
notStartedTag.tag.text=Not started
cannotStartTag.tag.text=Cannot start yet



dynamicExample.heading.heading=Dynamic Task Lists

dynamicExample.paragraph.html=But typically we're going to want to build task lists dynamically based upon the end-user's \
  progress through their user journey. So instead we can build up both <code>TaskListSection</code>s and <code>TaskListItem</code>s \
  from a combination of the form definition itself and the contents of <code>FormData</code>.

dynamicHowTo.paragraph.html=Using a task list assumes you're going to want to allow end users to jump around between \
  different stages within the form. We can support this by putting our questions within <code>Page</code>s instead of \
  <code>Section</code>s. We can then get even more clever by adding the <code>IncludeInTaskList</code> trait to every \
 <code>Page</code> which should be included in the Task List. This allows each individual <code>Page</code> to tell us \
  its status on the task list, as well as be in control of whether it should have a link to navigate to it or not \
  (steps within the task list that are not yet permitted to be visited should be displayed as plain text instead of a link). \
  Once this is done, then we can build up our <code>TaskList</code> like so:
dynamicHowTo.pre.text=TaskList.taskList("dli",\n  \
  pageMessages,\n  \
  TaskList.buildFromPages(pageMessages) _\n\
  )
dynamicPageOrder.paragraph.html=The order of sections within the task list is taken from the order in which the <code>Page</code>s are declared \
  in your <code>FormDefinition</code>.
dynamicPageDefinition.paragraph.html=And your definition of a <code>Page</code> could look something like this:
dynamicPageImplementation.pre.text=questionPageInTaskList(urlPath, pageMessages,\n  \
  PageDetails(),\n  \
  "taskListSectionKey",\n  \
  taskListItemFunction(pageMessages),\n  \
  Seq(\n    \
    BackLink.linkToLatestSection("backToLatestSection", pageMessages)\n  \
  ),\n  \
  Input.inputWholeNumber("in", pageMessages, required()),\n  \
  Button.button("continue", pageMessages)\n\
 )\n\
  \n\
 ...\n\
  \n\
 def taskListItemFunction[M <: Messages[M], F <: Messages[F]]\n  \
  (messages: Future[Messages[M]])\n  \
  (implicit formLevelMessages: Future[Messages[F]], renderer: TemplateRenderer):\n  \
  (Form, ValidationErrors, RequestInfo) => TaskListItem = (form, _, _) => {\n    \
  val linkToPage = Link.toPage("linkToPage", messages, urlPath)\n    \
  Input.extract("in", data) match {\n      \
    case None => TaskListItem(linkToPage,\n        \
        Tag.tag("notStartedTag", messages, TaskList.rightAlignTag.merge(Tag.grey)))\n      \
    case Some(_) => TaskListItem(linkToPage,\n        \
        Tag.tag("completeTag", messages, TaskList.rightAlignTag))\n    \
  }\n\
 }


taskListContinue.button.text=Continue