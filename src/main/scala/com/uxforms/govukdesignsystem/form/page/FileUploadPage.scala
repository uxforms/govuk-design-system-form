package com.uxforms.govukdesignsystem.form.page

import java.util.Locale

import com.uxforms.domain.constraint.FileUploadWithAllFormDataConstraint.noFileUploadWithAllFormDataConstraints
import com.uxforms.domain.constraint.FileUploadWithAllFormDataContentTypeMatches
import com.uxforms.domain.constraint.Required.required
import com.uxforms.domain.{Messages, ResourceBundleMessages}
import com.uxforms.dsl.helpers.ContainerHelper.{withReceivers, withRedirectAfterSubmissions, withRedirectBeforeRender}
import com.uxforms.dsl.helpers.VisibilityDSL.showWhenWidget
import com.uxforms.dsl.widgets.file.FileUploadActionNames
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper._
import com.uxforms.govukdesignsystem.dsl.widget._
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.{RedirectToPageAfterSubmission, RequireInviteCodeToAccess}

import scala.concurrent.{ExecutionContext, Future}

object FileUploadPage {

  val urlPath = "file-upload"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, executionContext: ExecutionContext, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/fileUpload", implicitly[ClassLoader], implicitly[Locale]))
    val visibilityChoices = Future.successful(ResourceBundleMessages.utf8("visibility", implicitly[ClassLoader], implicitly[Locale]))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess) ++
        withRedirectAfterSubmissions(RedirectToPageAfterSubmission(urlPath)) ++
        withReceivers(Seq(FileUploadInline.clearUploadedInlineFile("inlineFile"))),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("fileUploadHeading", pageMessages),
      Paragraph.body("fileUploadPara", pageMessages),
      Paragraph.body("fileUploadCode", pageMessages),

      FileUpload.fileUpload("file", pageMessages, noConstraints, noFileUploadWithAllFormDataConstraints),
      Button.input(FileUploadActionNames.uploadAction("file"), pageMessages),

      SectionBreak.xl("fileSummaryBreak"),
      Heading.m("fileSummaryHeading", pageMessages),
      Paragraph.body("fileSummaryPara", pageMessages),
      FileUpload.fileSummaryList("fileSummary", "file", pageMessages),


      SectionBreak.xl("validationBreak"),
      Heading.m("fileUploadValidation", pageMessages),
      Paragraph.body("fileUploadValidationPara", pageMessages),
      FileUpload.fileUpload("fileUploadValid", pageMessages, required()(Messages.empty()), FileUploadWithAllFormDataContentTypeMatches.contentTypeMatches("image/*".r)(Messages.empty()), extraTemplateRenderArgs = FileUpload.accept("image/*")),
      FileUpload.fileSummaryList("validSummary", "fileUploadValid", pageMessages),
      Button.input(FileUploadActionNames.uploadAction("fileUploadValid"), pageMessages),
      SectionBreak.s("validateBreak"),
      Button.button("validateFileUpload", pageMessages),

      SectionBreak.xl("visibilityBreak"),
      Heading.m("fileUploadVisibility", pageMessages),
      Paragraph.body("visibilityPara", pageMessages),
      Checkboxes.checkboxes("toggleFileUploadVisibility", pageMessages, noConstraints, visibilityChoices),
      FileUpload.fileUpload("fileUploadVisibility", pageMessages, noConstraints, noFileUploadWithAllFormDataConstraints, visibility = showWhenWidget("toggleFileUploadVisibility") hasValue ("VISIBLE")),

      SectionBreak.xl("inlineFileBreak"),
      Heading.m("inlineFileHeading", pageMessages),
      Paragraph.body("inlineFilePara", pageMessages),
      Paragraph.body("inlineFileCode", pageMessages),
      FileUploadInline.fileUploadInline("inlineFile", pageMessages, noConstraints, noFileUploadWithAllFormDataConstraints, visibility = FileUploadInline.showWhenNoFile("inlineFile")),
      FileUploadInline.fileUploadInlineSummaryList("inlineSummary", "inlineFile", pageMessages, visibility = FileUploadInline.showWhenHasFile("inlineFile")),


      SectionBreak.xl("validateBreak"),

      Button.button("validateFileUpload", pageMessages)
    )

  }
}
