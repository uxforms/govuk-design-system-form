package com.uxforms.govukdesignsystem.form.page

import java.util.Locale
import com.uxforms.domain.{Messages, ResourceBundleMessages}
import com.uxforms.dsl.helpers.ContainerHelper.withRedirectBeforeRender
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.widget.{BackLink, Heading, Paragraph, Pre, WarningText}
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.RequireInviteCodeToAccess

import scala.concurrent.Future

object WarningTextPage {

  val urlPath = "warningText"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/warningText", implicitly[ClassLoader], implicitly[Locale]))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("warningTextHeading", pageMessages),

      Paragraph.body("paraWarningText", pageMessages),
      Pre.pre("warningWidget", pageMessages),

      Paragraph.body("paraWarningMessages", pageMessages),
      Pre.pre("warningMessages", pageMessages),

      WarningText.warningText("warning", pageMessages)
    )

  }
}
