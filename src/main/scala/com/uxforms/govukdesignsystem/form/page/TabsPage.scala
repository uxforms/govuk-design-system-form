package com.uxforms.govukdesignsystem.form.page

import java.util.Locale
import com.uxforms.domain.{Messages, ResourceBundleMessages}
import com.uxforms.dsl.helpers.ContainerHelper.{withReceivers, withRedirectAfterSubmissions, withRedirectBeforeRender}
import com.uxforms.dsl.helpers.VisibilityDSL.showWhenWidget
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper.noConstraints
import com.uxforms.govukdesignsystem.dsl.styles.Spacing
import com.uxforms.govukdesignsystem.dsl.styles.Spacing.direction
import com.uxforms.govukdesignsystem.dsl.widget._
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.RadiosPage.urlPath
import com.uxforms.govukdesignsystem.form.page.support.{ClearWidgetValues, RedirectToPageAfterSubmission, RequireInviteCodeToAccess}

import scala.concurrent.{ExecutionContext, Future}

object TabsPage {

  val urlPath = "tabs"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, executionContext: ExecutionContext, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/tabs", implicitly[ClassLoader], implicitly[Locale]))
    val visibilityChoices = Future.successful(ResourceBundleMessages.utf8("visibility", implicitly[ClassLoader], implicitly[Locale]))

    val tabContents = Seq(
      Paragraph.body("tab1Para", pageMessages),
      Paragraph.body("tab2Para", pageMessages)
    )

    val topMargin = Heading.classes(Spacing.responsiveMargin(9, direction.top))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess) ++
        withReceivers(Seq(ClearWidgetValues)) ++
        withRedirectAfterSubmissions(RedirectToPageAfterSubmission(urlPath)),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("tabsHeading", pageMessages),
      Paragraph.body("defaultTabs", pageMessages),
      Paragraph.body("defaultTabsWidget", pageMessages),
      Pre.pre("defaultTabsWidget", pageMessages, Pre.isCode),
      Paragraph.body("defaultTabsMsgs", pageMessages),
      Pre.pre("defaultTabsMsgs", pageMessages, Pre.isCode),
      Tabs.tabs("defaultTabs", pageMessages, tabContents),

      Heading.m("persistedHeading", pageMessages, topMargin),
      Paragraph.body("persisted", pageMessages),
      Pre.pre("persisted", pageMessages, Pre.isCode),
      Tabs.persistedTabs("persistedTabs", pageMessages, tabContents),
      Div.div(
        Div.classes(Button.buttonGroupClass),
        Button.button("saveState", pageMessages),
        Link.toPage("resetState", pageMessages, s"$urlPath?${ClearWidgetValues.requestParameterName}=persistedTabs")
      ),

      Heading.m("visibilityHeading", pageMessages, topMargin),
      Paragraph.body("visibilityPara", pageMessages),
      Checkboxes.checkboxes("toggleTabsVisibility", pageMessages, noConstraints, visibilityChoices),
      Tabs.tabs("visibilityTabs", pageMessages, tabContents,
        visibility = showWhenWidget("toggleTabsVisibility") hasValue ("VISIBLE"))
    )
  }
}
