package com.uxforms.govukdesignsystem.form.page

import com.uxforms.domain.{Messages, ResourceBundleMessages}
import com.uxforms.dsl.helpers.ContainerHelper.withRedirectBeforeRender
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.widget.{BackLink, Heading, Paragraph, Pre}
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.RequireInviteCodeToAccess

import java.util.Locale
import scala.concurrent.Future

object PrePage {

  val urlPath = "pre"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/pre", implicitly[ClassLoader], implicitly[Locale]))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("stylesHeading", pageMessages),

      Heading.l("pre", pageMessages),

      Paragraph.body("preIntro", pageMessages),
      Paragraph.body("preDefault", pageMessages),
      Pre.pre("preDefault", pageMessages, Pre.isCode),
      Paragraph.body("preDefaultMsgs", pageMessages),
      Pre.pre("preDefaultMsgs", pageMessages, Pre.isCode),

      Heading.m("codeHeading", pageMessages),
      Paragraph.body("codeBlocks", pageMessages),
      Pre.pre("codeBlocks", pageMessages, Pre.isCode)
    )
  }


}
