package com.uxforms.govukdesignsystem.form.page

import com.uxforms.domain.constraint.FixedChoice
import com.uxforms.domain.{Messages, ResourceBundleMessages}
import com.uxforms.dsl.helpers.ContainerHelper.{withReceivers, withRedirectAfterSubmissions, withRedirectBeforeRender}
import com.uxforms.dsl.helpers.VisibilityDSL.showWhenWidget
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper.{constraintAppender, noConstraints}
import com.uxforms.govukdesignsystem.dsl.styles.Spacing
import com.uxforms.govukdesignsystem.dsl.styles.Spacing.direction
import com.uxforms.govukdesignsystem.dsl.template.Label
import com.uxforms.govukdesignsystem.dsl.widget._
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.{RedirectToPageAfterSubmission, RequireInviteCodeToAccess}
import com.uxforms.govukdesignsystem.form.widget.ChangeFormDataButton
import play.api.libs.json.{JsNull, Json}

import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

object SelectPage {

  val urlPath = "select"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, executionContext: ExecutionContext, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/select", implicitly[ClassLoader], implicitly[Locale]))
    val visibilityChoices = Future.successful(ResourceBundleMessages.utf8("visibility", implicitly[ClassLoader], implicitly[Locale]))
    val countryChoices = Future.successful(ResourceBundleMessages.utf8("countries", implicitly[ClassLoader], implicitly[Locale]))
    val yesNoChoices = Future.successful(ResourceBundleMessages.utf8("yesNo", implicitly[ClassLoader], implicitly[Locale]))

    val headingMarginTop = Heading.classes(Spacing.responsiveMargin(9, direction.top))

    val randomisedSelect = Select.selectWithRandomisedItemOrder("randomisedSelect", pageMessages, noConstraints, countryChoices, "randomisedSelectOrder")

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess) ++
        withReceivers(Seq(randomisedSelect.randomiseAndPersistItemOrder)) ++
        withRedirectAfterSubmissions(RedirectToPageAfterSubmission(urlPath)),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("selectHeading", pageMessages),
      Paragraph.body("defaultSelect", pageMessages),
      Select.select("defaultSelect", pageMessages, noConstraints, countryChoices),

      Heading.m("selectWithConstraint", pageMessages, headingMarginTop),
      Paragraph.body("selectWithConstraint", pageMessages),
      Select.select("selectWithConstraint", pageMessages, new FixedChoice(Set("NO"), Messages.empty()), yesNoChoices),
      Button.button("selectValidate", pageMessages),

      Heading.m("selectWithHint", pageMessages, headingMarginTop),
      Paragraph.body("selectWithHint", pageMessages),
      Select.select("selectWithHint", pageMessages, noConstraints, yesNoChoices, Label.asPageHeading),

      Heading.m("randomised", pageMessages, headingMarginTop),
      Paragraph.body("randomised", pageMessages),
      Paragraph.body("randomisedHow", pageMessages),
      Pre.pre("randomisedHow", pageMessages, Pre.isCode),
      Paragraph.body("randomisedReceive", pageMessages),
      Pre.pre("randomisedReceive", pageMessages, Pre.isCode),
      Paragraph.body("randomisedFixed", pageMessages),
      Pre.pre("randomisedFixed", pageMessages, Pre.isCode),
      randomisedSelect,
      new ChangeFormDataButton(
        "reRandomise",
        (_, existingData) => existingData + Json.obj(randomisedSelect.randomisedItemOrderKey -> JsNull),
        Button.button("reRandomise", pageMessages, Button.secondary)
      ),


      Heading.m("selectVisibilityHeading", pageMessages, headingMarginTop),
      Paragraph.body("selectVisibilityPara", pageMessages),
      Checkboxes.checkboxes("toggleButtonVisibility", pageMessages, noConstraints, visibilityChoices),
      Select.select("selectButton", pageMessages, noConstraints, yesNoChoices, visibility = showWhenWidget("toggleButtonVisibility") hasValue ("VISIBLE"))
    )
  }
}
