package com.uxforms.govukdesignsystem.form.page

import java.util.Locale

import com.uxforms.domain.constraint.Required.required
import com.uxforms.domain.{Messages, ResourceBundleMessages}
import com.uxforms.dsl.helpers.ContainerHelper.withRedirectBeforeRender
import com.uxforms.dsl.helpers.VisibilityDSL.showWhenWidget
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper._
import com.uxforms.govukdesignsystem.dsl.widget.Input.inputText
import com.uxforms.govukdesignsystem.dsl.widget._
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.RequireInviteCodeToAccess

import scala.concurrent.{ExecutionContext, Future}

object FieldsetPage {

  val urlPath = "fieldset"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, ec: ExecutionContext, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/fieldset", implicitly[ClassLoader], implicitly[Locale]))
    val visibilityChoices = Future.successful(ResourceBundleMessages.utf8("visibility", implicitly[ClassLoader], implicitly[Locale]))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("fieldsetHeading", pageMessages),
      Paragraph.body("fieldsetIntro", pageMessages),

      Fieldset.fieldset("myFieldset", pageMessages, Fieldset.asPageHeading,
        inputText("addressLine1", pageMessages, required()(Messages.empty())),
        inputText("addressLine2", pageMessages, noConstraints),
        inputText("townInput", pageMessages, noConstraints, Input.widthTwoThirds),
        inputText("countryInput", pageMessages, noConstraints, Input.widthTwoThirds),
        inputText("postcodeInput", pageMessages, noConstraints, Input.width10)
      ),

      SectionBreak.xl("visibilityBreak"),
      Heading.m("fieldsetVisibility", pageMessages),
      Paragraph.body("visibilityPara", pageMessages),
      Checkboxes.checkboxes("toggleFieldsetVisibility", pageMessages, noConstraints, visibilityChoices),
      Fieldset.fieldset("hiddenFieldset", pageMessages, showWhenWidget("toggleFieldsetVisibility") hasValue ("VISIBLE"),
        inputText("shownInput", pageMessages, required()(Messages.empty()), Input.width10)
      ),

      SectionBreak.m("validateBreak"),
      Button.button("fieldsetValidate", pageMessages)
    )
  }
}
