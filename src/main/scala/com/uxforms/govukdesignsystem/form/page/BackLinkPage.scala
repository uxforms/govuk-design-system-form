package com.uxforms.govukdesignsystem.form.page

import java.util.Locale

import com.uxforms.domain.{Messages, ResourceBundleMessages}
import com.uxforms.dsl.helpers.ContainerHelper.withRedirectBeforeRender
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.widget.{BackLink, Heading, Paragraph, WarningText}
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.RequireInviteCodeToAccess
import com.uxforms.govukdesignsystem.form.section.{Browse, Welcome}

import scala.concurrent.Future

object BackLinkPage {

  val urlPath = "back-link"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/backLink", implicitly[ClassLoader], implicitly[Locale]))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("backLinkHeading", pageMessages),
      Paragraph.body("backLinkIntro", pageMessages),

      Heading.m("backLinkToLatestSection", pageMessages),
      Paragraph.body("backLinkToLatestSectionPara", pageMessages),
      BackLink.linkToLatestSection("linkToLatestSection", pageMessages),

      Heading.m("linkToEarlierSection", pageMessages),
      Paragraph.body("linkToEarlierSectionPara", pageMessages),
      BackLink.linkToEarlierSection("linkToEarlierSection", pageMessages, Welcome.name),

      Heading.m("buttonToPreviousSection", pageMessages),
      Paragraph.body("buttonToPreviousSectionPara", pageMessages),
      BackLink.buttonToPreviousSection("buttonToPreviousSection", pageMessages)
    )

  }
}
