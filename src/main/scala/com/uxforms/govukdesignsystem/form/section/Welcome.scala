package com.uxforms.govukdesignsystem.form.section

import java.util.Locale
import com.uxforms.domain.constraint.Required.required
import com.uxforms.domain.env.EnvironmentHelper
import com.uxforms.domain.{FormData, Messages, ResourceBundleMessages}
import com.uxforms.dsl.containers.Section
import com.uxforms.dsl.containers.mustache.Section.section
import com.uxforms.dsl.helpers.ContainerBuilders.SectionDetails
import com.uxforms.dsl.widgets.BareTemplate.paragraph
import com.uxforms.dsl.widgets.{Heading, WidgetVisibility}
import com.uxforms.dsl.widgets.Input.inputText
import com.uxforms.dsl.{MustacheRenderEngine, RemoteTemplateResolver}

import scala.concurrent.Future

object Welcome {

  val name = "welcome"

  // Make this section look like a regular UX Forms demo form as it is visible to the public (and Google's crawl bot!)
  private implicit val renderEngine: MustacheRenderEngine = new MustacheRenderEngine(new RemoteTemplateResolver("uxforms", "templates"))

  def apply[F <: Messages[F]]()
                             (implicit formLevelMessages: Future[Messages[F]], classLoader: ClassLoader, locale: Locale): Section = {

    implicit val messages: ResourceBundleMessages = ResourceBundleMessages.utf8("section/welcome", classLoader, locale)

    section(
      name,
      messages,
      SectionDetails(),
      Heading.xlarge("welcomeTitle", messages),
      paragraph("introduction", messages),
      paragraph("toContinue", messages),
      inputText("inviteCode", messages, required(), onlyShowWhenNotLocal)
    )
  }

  val onlyShowWhenNotLocal = new WidgetVisibility {
    override def shown(data: FormData): Boolean = new EnvironmentHelper().env != "local"
  }

  def hasInviteCode(data: FormData): Boolean = {
    (data.textData \ "inviteCode").asOpt[String].exists(_.nonEmpty)
  }

}
