package com.uxforms.govukdesignsystem.form.page

import java.util.Locale
import com.uxforms.domain.{Messages, ResourceBundleMessages}
import com.uxforms.dsl.helpers.ContainerHelper.withRedirectBeforeRender
import com.uxforms.govukdesignsystem.dsl.Page.errorPage
import com.uxforms.govukdesignsystem.dsl.styles.Spacing
import com.uxforms.govukdesignsystem.dsl.styles.Spacing.direction
import com.uxforms.govukdesignsystem.dsl.widget.{BackLink, Heading, Paragraph, Pre, SectionBreak}
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.RequireInviteCodeToAccess

import scala.concurrent.Future

object ServiceUnavailablePage {

  val urlPath: String = "service-unavailable"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/serviceUnavailable", implicitly[ClassLoader], implicitly[Locale]))

    errorPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess),

      BackLink.linkToLatestSection("backToLatestSection", pageMessages),

      Heading.l("serviceUnavailableHeading", pageMessages, Heading.classes(Spacing.responsiveMargin(9, direction.top))),
      Paragraph.body("availability", pageMessages),
      Paragraph.body("contact", pageMessages),

      SectionBreak.xl("serviceUnavailableBreak", SectionBreak.visible),

      Paragraph.body("page", pageMessages),
      Pre.pre("page", pageMessages, Pre.isCode),
      Paragraph.body("pageMsg", pageMessages),
      Pre.pre("pageMsg", pageMessages, Pre.isCode)
    )

  }


}
