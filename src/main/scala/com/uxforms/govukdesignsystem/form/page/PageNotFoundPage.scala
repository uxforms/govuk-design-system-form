package com.uxforms.govukdesignsystem.form.page

import java.util.Locale

import com.uxforms.domain._
import com.uxforms.dsl.helpers.ContainerHelper.withRedirectBeforeRender
import com.uxforms.govukdesignsystem.dsl.Page.errorPage
import com.uxforms.govukdesignsystem.dsl.widget.{BackLink, Heading, Paragraph, SectionBreak}
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.RequireInviteCodeToAccess

import scala.concurrent.Future

object PageNotFoundPage {

  val urlPath: String = "page-not-found-pages"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/pageNotFound", implicitly[ClassLoader], implicitly[Locale]))

    errorPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess),

      BackLink.linkToLatestSection("backToLatestSection", pageMessages),

      Heading.l("pageNotFoundHeading", pageMessages),
      Paragraph.body("typed", pageMessages),
      Paragraph.body("pasted", pageMessages),
      Paragraph.body("correct", pageMessages),

      SectionBreak.xl("pageNotFoundBreak", SectionBreak.visible),
      Paragraph.body("pageNotFoundPage", pageMessages),
      Paragraph.body("pageNotFoundMessages", pageMessages)
    )

  }

}