package com.uxforms.govukdesignsystem.form.widget

import com.uxforms.domain
import com.uxforms.domain._
import com.uxforms.domain.widget._
import com.uxforms.dsl.Form
import com.uxforms.govukdesignsystem.dsl.widget.Content

import java.io.File
import scala.concurrent.{ExecutionContext, Future}

class ChangeFormDataButton[M <: Messages[M], F <: Messages[F]](val name: String, changeData: (Map[String, Seq[String]], FormData) => FormData, button: Content[M, F])
  extends Widget with Submittable with Persisted {


  override def validate(data: FormData)(implicit ec: ExecutionContext): Future[ValidationResult] =
    Future.successful(ValidationResult(data, Seq.empty))

  override def merge(formPostData: Map[String, Seq[String]], existingFormData: FormData): FormData = {
    if (hasSubmittedTheForm(formPostData)) {
      changeData(formPostData, existingFormData)
    } else existingFormData
  }

  override def hasSubmittedTheForm(postedFormData: Map[String, Seq[String]]): Boolean =
    postedFormData.contains(name)

  override def validateSubmission(postedFormData: Map[String, Seq[String]], existingFormData: FormData, file: Option[File])(implicit ec: ExecutionContext): Future[domain.ValidationResult] = {
    Future.successful(ValidationResult(existingFormData, Seq.empty))
  }

  override def render(form: Form, errors: ValidationErrors, requestInfo: RequestInfo)(implicit executionContext: ExecutionContext): Future[String] =
    button.render(form, errors, requestInfo)
}
