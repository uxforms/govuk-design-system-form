package com.uxforms.govukdesignsystem.form.page

import java.util.Locale
import com.uxforms.domain.{Messages, ResourceBundleMessages}
import com.uxforms.dsl.helpers.ContainerHelper.withRedirectBeforeRender
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.styles.Spacing
import com.uxforms.govukdesignsystem.dsl.styles.Spacing.direction
import com.uxforms.govukdesignsystem.dsl.widget.{BackLink, Heading, Paragraph, Pre, SectionBreak}
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.RequireInviteCodeToAccess

import scala.concurrent.Future

object TypographyPage {

  val urlPath = "typography"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/typography", implicitly[ClassLoader], implicitly[Locale]))

    val marginTop9 = Heading.classes(Spacing.responsiveMargin(9, direction.top))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("typographyHeading", pageMessages),

      Heading.l("headings", pageMessages),
      Paragraph.body("widgets", pageMessages),
      Pre.pre("widgets", pageMessages, Pre.isCode),
      Paragraph.body("messages", pageMessages),
      Pre.pre("messages", pageMessages, Pre.isCode),

      Heading.xl("first", pageMessages),
      Heading.l("second", pageMessages),
      Heading.m("third", pageMessages),
      Heading.s("fourth", pageMessages),


      Heading.l("captionedHeadings", pageMessages, marginTop9),
      Paragraph.body("captionedHeadings", pageMessages),
      Pre.pre("captionedHeadings", pageMessages, Pre.isCode),
      Heading.xl("a", pageMessages),
      Heading.l("b", pageMessages),
      Heading.m("c", pageMessages),
      Heading.s("d", pageMessages),


      Heading.l("paragraphs", pageMessages, marginTop9),

      Heading.m("headingBody", pageMessages),
      Paragraph.body("bodyWidget", pageMessages),
      Pre.pre("bodyWidget", pageMessages, Pre.isCode),
      Paragraph.body("bodyMessages", pageMessages),
      Pre.pre("bodyMessages", pageMessages, Pre.isCode),
      Paragraph.body("b", pageMessages),

      Heading.m("headingLead", pageMessages),
      Pre.pre("paraLead", pageMessages, Pre.isCode),
      Paragraph.lead("lead", pageMessages),

      Heading.m("headingSmall", pageMessages),
      Pre.pre("paraSmall", pageMessages, Pre.isCode),
      Paragraph.small("small", pageMessages),


      Heading.l("sectionBreak", pageMessages, marginTop9),
      Paragraph.body("sectionBreakPara", pageMessages),
      Pre.pre("sectionBreakWidget", pageMessages),
      SectionBreak.xl("xlBreak", SectionBreak.visible),
      SectionBreak.l("xlBreak", SectionBreak.visible),
      SectionBreak.m("xlBreak", SectionBreak.visible),
      SectionBreak.s("xlBreak", SectionBreak.visible)
    )
  }


}
