package com.uxforms.govukdesignsystem.form.page

import com.uxforms.domain.{Messages, ResourceBundleMessages, SimpleExtraTemplateRenderArgs}
import com.uxforms.dsl.helpers.ContainerHelper.withRedirectBeforeRender
import com.uxforms.dsl.helpers.VisibilityDSL.showWhenWidget
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper.noConstraints
import com.uxforms.govukdesignsystem.dsl.styles.Spacing
import com.uxforms.govukdesignsystem.dsl.widget._
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.RequireInviteCodeToAccess

import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object DetailsPage {

  val urlPath = "details"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/details", implicitly[ClassLoader], implicitly[Locale]))
    val visibilityChoices = Future.successful(ResourceBundleMessages.utf8("visibility", implicitly[ClassLoader], implicitly[Locale]))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("detailsHeading", pageMessages),
      Paragraph.body("detailsIntro", pageMessages),
      Pre.pre("detailsWidget", pageMessages, Pre.isCode),
      Paragraph.body("detailsMessages", pageMessages),
      Pre.pre("detailsMessages", pageMessages, Pre.isCode),

      Details.details("details", pageMessages),

      Heading.m("detailsVisibility", pageMessages, Heading.classes(Spacing.responsiveMargin(9, Spacing.direction.top))),
      Paragraph.body("visibilityPara", pageMessages),

      Checkboxes.checkboxes("toggleDetailsVisibility", pageMessages, noConstraints, visibilityChoices),
      Details.details("details", pageMessages, SimpleExtraTemplateRenderArgs("details.open" -> "open"), showWhenWidget("toggleDetailsVisibility") hasValue ("VISIBLE"))
    )

  }
}
