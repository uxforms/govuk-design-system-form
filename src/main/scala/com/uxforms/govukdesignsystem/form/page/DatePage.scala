package com.uxforms.govukdesignsystem.form.page

import com.uxforms.domain.constraint.MaxDate.maxDate
import com.uxforms.domain.constraint.MinDate.minDate
import com.uxforms.domain.constraint.Required.required
import com.uxforms.domain.{EmptyMessages, Messages, NoExtraRenderArgs, ResourceBundleMessages}
import com.uxforms.dsl.helpers.ConstraintHelper
import com.uxforms.dsl.helpers.ConstraintHelper.javaLocalDateToPartialDate
import com.uxforms.dsl.helpers.ContainerHelper.{withRedirectAfterSubmissions, withRedirectBeforeRender}
import com.uxforms.dsl.helpers.VisibilityDSL.showWhenWidget
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper._
import com.uxforms.govukdesignsystem.dsl.constraint.FullDate.fullDate
import com.uxforms.govukdesignsystem.dsl.widget._
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.{RedirectToPageAfterSubmission, RequireInviteCodeToAccess}

import java.time.LocalDate
import java.util.Locale
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


object DatePage {

  val urlPath = "date-input"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/date", implicitly[ClassLoader], implicitly[Locale]))
    val visibilityChoices = Future.successful(ResourceBundleMessages.utf8("visibility", implicitly[ClassLoader], implicitly[Locale]))

    implicit val emptyMessages: EmptyMessages = Messages.empty()

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess) ++
        withRedirectAfterSubmissions(RedirectToPageAfterSubmission(urlPath)),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("dateHeading", pageMessages),

      Paragraph.body("dateIntro", pageMessages),
      Paragraph.body("dateDefaults", pageMessages),
      DateInput.date("myDate", pageMessages, Seq.empty),

      SectionBreak.xl("autocompleteBreak"),
      Heading.m("dateOfBirthHeading", pageMessages),
      Paragraph.body("dateOfBirthPara", pageMessages),
      DateInput.dateOfBirth("dob", pageMessages, fullDate() ++ ConstraintHelper.dateOfBirth(), DateInput.asPageHeading),

      SectionBreak.xl("validationBreak"),
      Heading.m("dateValidation", pageMessages),
      Paragraph.body("dateValidationPara", pageMessages),
      DateInput.date("passportIssued", pageMessages, required() ++ fullDate() ++ minDate(LocalDate.now().minusYears(20)) ++ maxDate(LocalDate.now())),

      SectionBreak.xl("visibilityBreak"),
      Heading.m("detailsVisibility", pageMessages),
      Paragraph.body("visibilityPara", pageMessages),
      Checkboxes.checkboxes("toggleDetailsVisibility", pageMessages, noConstraints, visibilityChoices),
      DateInput.date("visibilityDate", pageMessages, noConstraints, NoExtraRenderArgs, showWhenWidget("toggleDetailsVisibility") hasValue ("VISIBLE")),

      SectionBreak.m("validateBreak"),
      Button.button("dateValidate", pageMessages)
    )

  }
}
