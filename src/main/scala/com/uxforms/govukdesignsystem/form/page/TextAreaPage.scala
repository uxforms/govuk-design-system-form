package com.uxforms.govukdesignsystem.form.page

import com.uxforms.domain.constraint.Required.required
import com.uxforms.domain.{Messages, ResourceBundleMessages}
import com.uxforms.dsl.helpers.ContainerHelper.{withRedirectAfterSubmissions, withRedirectBeforeRender}
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper._
import com.uxforms.govukdesignsystem.dsl.styles.Spacing
import com.uxforms.govukdesignsystem.dsl.template.Label
import com.uxforms.govukdesignsystem.dsl.widget._
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.{RedirectToPageAfterSubmission, RequireInviteCodeToAccess}

import java.util.Locale
import scala.concurrent.Future

object TextAreaPage {

  val urlPath = "text-area"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/textArea", implicitly[ClassLoader], implicitly[Locale]))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess) ++
        withRedirectAfterSubmissions(RedirectToPageAfterSubmission(urlPath)),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("textAreaHeading", pageMessages),

      Paragraph.body("paraWithoutHeading", pageMessages),
      Pre.pre("paraWithoutHeading", pageMessages, Pre.isCode),
      Paragraph.body("paraWithoutHeadingMsgs", pageMessages),
      Pre.pre("paraWithoutHeadingMsgs", pageMessages),
      TextArea.textArea("textAreaWithoutHeading", pageMessages, required()(Messages.empty())),

      Heading.m("headings", pageMessages, Heading.classes(Spacing.responsiveMargin(9, Spacing.direction.top))),

      Paragraph.body("paraForHeading", pageMessages),
      Pre.pre("paraForHeading", pageMessages),
      TextArea.textArea("textAreaWithHeading", pageMessages, noConstraints, Label.asPageHeading),

      Heading.m("sizing", pageMessages, Heading.classes(Spacing.responsiveMargin(9, Spacing.direction.top))),

      Paragraph.body("sizing", pageMessages),
      Pre.pre("sizing", pageMessages),
      TextArea.textArea("sa", pageMessages, noConstraints, TextArea.rows(15)),

      Button.button("textAreaValidate", pageMessages)
    )
  }

}
