package com.uxforms.govukdesignsystem.form.page

import java.util.Locale
import com.uxforms.domain.{Messages, ResourceBundleMessages}
import com.uxforms.dsl.helpers.ContainerHelper.withRedirectBeforeRender
import com.uxforms.dsl.helpers.VisibilityDSL.showWhenWidget
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper.noConstraints
import com.uxforms.govukdesignsystem.dsl.styles.Spacing
import com.uxforms.govukdesignsystem.dsl.styles.Spacing.direction
import com.uxforms.govukdesignsystem.dsl.widget._
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.RequireInviteCodeToAccess

import scala.concurrent.{ExecutionContext, Future}

object ButtonPage {

  val urlPath = "button"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, executionContext: ExecutionContext, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/button", implicitly[ClassLoader], implicitly[Locale]))
    val visibilityChoices = Future.successful(ResourceBundleMessages.utf8("visibility", implicitly[ClassLoader], implicitly[Locale]))

    val topMargin = Heading.classes(Spacing.responsiveMargin(6, direction.top))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("buttonHeading", pageMessages),

      Heading.m("default", pageMessages),
      Pre.pre("defaultButton", pageMessages, Pre.isCode),
      Paragraph.body("defaultMsgs", pageMessages),
      Pre.pre("defaultMsgs", pageMessages, Pre.isCode),
      Button.button("defaultButton", pageMessages),

      Heading.m("start", pageMessages, topMargin),
      Pre.pre("start", pageMessages, Pre.isCode),
      Button.link("startButton", pageMessages, Button.asStart ++ Button.pageUrlArg(urlPath)),

      Heading.m("secondary", pageMessages, topMargin),
      Pre.pre("secondary", pageMessages, Pre.isCode),
      Button.button("secondaryButton", pageMessages, Button.secondary),

      Heading.m("warning", pageMessages, topMargin),
      Pre.pre("warning", pageMessages, Pre.isCode),
      Button.button("warningButton", pageMessages, Button.warning),

      Heading.m("disabled", pageMessages, topMargin),
      Pre.pre("disabled", pageMessages, Pre.isCode),
      Button.button("disabledButton", pageMessages, Button.disabled),

      Heading.m("grouping", pageMessages, topMargin),
      Pre.pre("grouping", pageMessages, Pre.isCode),
      Div.div(
        Div.classes(Button.buttonGroupClass),
        Button.button("save", pageMessages),
        Button.button("draft", pageMessages, Button.secondary)
      ),
      Paragraph.body("groupingLink", pageMessages),
      Pre.pre("groupingLink", pageMessages, Pre.isCode),
      Div.div(
        Div.classes(Button.buttonGroupClass),
        Button.button("continue", pageMessages),
        Link.toExternalUrl("cancel", pageMessages, "#")
      ),

      Heading.m("moreThanOnce", pageMessages, topMargin),
      Pre.pre("moreThanOnce", pageMessages, Pre.isCode),
      Button.button("preventDoubleClickButton", pageMessages, Button.preventDoubleClick()),

      Heading.m("link", pageMessages, topMargin),
      Pre.pre("link", pageMessages, Pre.isCode),
      Button.link("linkButton", pageMessages),

      Heading.m("input", pageMessages, topMargin),
      Pre.pre("input", pageMessages, Pre.isCode),
      Button.input("inputButton", pageMessages),

      Heading.m("buttonVisibility", pageMessages, topMargin),
      Paragraph.body("buttonVisibility", pageMessages),
      Pre.pre("buttonVisibility", pageMessages, Pre.isCode),
      Checkboxes.checkboxes("toggleButtonVisibility", pageMessages, noConstraints, visibilityChoices),
      Button.button("buttonVisibility", pageMessages, visibility = showWhenWidget("toggleButtonVisibility") hasValue ("VISIBLE"))
    )

  }
}
