package com.uxforms.govukdesignsystem.form.page

import java.util.Locale

import com.uxforms.domain.{Messages, ResourceBundleMessages}
import com.uxforms.dsl.helpers.ContainerHelper.withRedirectBeforeRender
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.widget.{BackLink, Heading, InsetText, Paragraph}
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.RequireInviteCodeToAccess

import scala.concurrent.Future

object InsetTextPage {

  val urlPath = "inset-text"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/insetText", implicitly[ClassLoader], implicitly[Locale]))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("insetTextHeading", pageMessages),
      Paragraph.body("insetTextIntro", pageMessages),

      InsetText.insetText("inset", pageMessages)
    )

  }
}
