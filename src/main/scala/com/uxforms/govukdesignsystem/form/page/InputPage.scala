package com.uxforms.govukdesignsystem.form.page

import java.util.Locale
import com.uxforms.domain.constraint.Required.required
import com.uxforms.domain.{Messages, ResourceBundleMessages}
import com.uxforms.dsl.helpers.ContainerHelper.withRedirectBeforeRender
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.constraint.ConstraintHelper._
import com.uxforms.govukdesignsystem.dsl.styles.Spacing
import com.uxforms.govukdesignsystem.dsl.styles.Spacing.direction
import com.uxforms.govukdesignsystem.dsl.template.Label
import com.uxforms.govukdesignsystem.dsl.widget.Input.{inputDecimalNumber, inputText, inputWholeNumber}
import com.uxforms.govukdesignsystem.dsl.widget._
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.RequireInviteCodeToAccess

import scala.concurrent.Future

object InputPage {

  val urlPath = "input"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/input", implicitly[ClassLoader], implicitly[Locale]))
    val topMargin = Heading.classes(Spacing.responsiveMargin(9, direction.top))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("inputHeading", pageMessages),

      Paragraph.body("text", pageMessages),
      Pre.pre("text", pageMessages, Pre.isCode),
      Paragraph.body("textMsg", pageMessages),
      Pre.pre("textMsg", pageMessages, Pre.isCode),
      inputText("defaultInput", pageMessages, required()(Messages.empty())),


      Paragraph.body("paraHeadingInput", pageMessages, Paragraph.classes(Spacing.responsiveMargin(9, direction.top))),
      Pre.pre("paraHeadingInput", pageMessages, Pre.isCode),
      inputText("inputWithHeading", pageMessages, noConstraints, Label.asPageHeading),


      Heading.s("fixedWidthHeading", pageMessages, topMargin),
      Pre.pre("fixedWidthExample", pageMessages, Pre.isCode),
      inputText("width20", pageMessages, noConstraints, Input.width20),
      inputText("width10", pageMessages, noConstraints, Input.width10),
      inputText("width5", pageMessages, noConstraints, Input.width5),
      inputText("width4", pageMessages, noConstraints, Input.width4),
      inputText("width3", pageMessages, noConstraints, Input.width3),
      inputText("width2", pageMessages, noConstraints, Input.width2),

      Heading.s("fluidWidthHeading", pageMessages, topMargin),
      Pre.pre("fluidWidthExample", pageMessages, Pre.isCode),
      inputText("widthFull", pageMessages, noConstraints, Input.widthFull),
      inputText("widthThreeQuarters", pageMessages, noConstraints, Input.widthThreeQuarters),
      inputText("widthTwoThirds", pageMessages, noConstraints, Input.widthTwoThirds),
      inputText("widthOneHalf", pageMessages, noConstraints, Input.widthOneHalf),
      inputText("widthOneThird", pageMessages, noConstraints, Input.widthOneThird),
      inputText("widthOneQuarter", pageMessages, noConstraints, Input.widthOneQuarter),

      Heading.m("numberHeading", pageMessages, topMargin),
      Pre.pre("wholeNumberExample", pageMessages, Pre.isCode),
      inputWholeNumber("wholeNumber", pageMessages, noConstraints, Input.width10),

      Pre.pre("decimalNumberExample", pageMessages, Pre.isCode ++ topMargin),
      inputDecimalNumber("decimalNumber", pageMessages, noConstraints, Input.width10),

      Heading.m("prefixSuffix", pageMessages, topMargin),
      Paragraph.body("prefixSuffix", pageMessages),
      Pre.pre("prefixSuffix", pageMessages, Pre.isCode),
      Paragraph.body("prefixSuffixMsg", pageMessages),
      Pre.pre("prefixSuffixMsg", pageMessages, Pre.isCode),
      inputText("prefixSuffixInput", pageMessages, noConstraints, Input.width5),

      Heading.s("prefix", pageMessages, topMargin),
      Pre.pre("prefix", pageMessages, Pre.isCode),
      Paragraph.body("prefixMsg", pageMessages),
      Pre.pre("prefixMsg", pageMessages, Pre.isCode),
      inputText("prefixInput", pageMessages, noConstraints, Input.width5),

      Heading.s("suffix", pageMessages, topMargin),
      Pre.pre("suffix", pageMessages),
      Paragraph.body("suffixMsg", pageMessages),
      Pre.pre("suffixMsg", pageMessages, Pre.isCode),
      inputText("suffixInput", pageMessages, noConstraints, Input.width5),

      Div.div(
        Div.classes(Spacing.responsiveMargin(9, direction.top)),
        Button.button("inputValidate", pageMessages)
      )
    )
  }
}
