package com.uxforms.govukdesignsystem.form.page

import java.util.Locale

import com.uxforms.domain._
import com.uxforms.dsl.Form
import com.uxforms.dsl.helpers.ContainerHelper.withRedirectBeforeRender
import com.uxforms.govukdesignsystem.dsl.Page.errorPage
import com.uxforms.govukdesignsystem.dsl.message.MessageHelper._
import com.uxforms.govukdesignsystem.dsl.widget.{BackLink, Heading, Paragraph, SectionBreak}
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.build.GOVUKDesignSystemFormDefinitionBuildInfo
import com.uxforms.govukdesignsystem.form.page.support.RequireInviteCodeToAccess

import scala.concurrent.Future
import scala.concurrent.duration.Duration

object ProblemWithServicePage {

  val urlPath: String = "problem-with-the-service-pages"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/problemWithService", implicitly[ClassLoader], implicitly[Locale]))

    errorPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess),

      BackLink.linkToLatestSection("backToLatestSection", pageMessages),

      Heading.l("problemWithServiceHeading", pageMessages),
      Paragraph.body("tryAgain", pageMessages),
      Paragraph.body("savedYourAnswers", pageMessages, applyNamedMessageValuesToMessageEntry(pageMessages, "savedYourAnswers.paragraph.text", (_: Form, _: ValidationErrors, _: RequestInfo) => {
        Map("hours" -> Duration(GOVUKDesignSystemFormDefinitionBuildInfo.retentionPeriod).toHours)
      })),
      Paragraph.body("contact", pageMessages),

      SectionBreak.xl("problemWithServiceBreak", SectionBreak.visible),
      Paragraph.body("problemWithServicePage", pageMessages),
      Paragraph.body("problemWithServiceMessages", pageMessages)
    )

  }

}