package com.uxforms.govukdesignsystem.form.section

import java.util.Locale
import com.uxforms.domain.{FormData, Messages, ResourceBundleMessages}
import com.uxforms.dsl.containers.SectionCanContinue
import com.uxforms.dsl.helpers.ContainerHelper._
import com.uxforms.govukdesignsystem.dsl.Section.section
import com.uxforms.govukdesignsystem.dsl.message.MessageHelper.applyNamedMessageValuesToMessageEntry
import com.uxforms.govukdesignsystem.dsl.widget.{Heading, Link, List, Paragraph, Pre, SectionBreak}
import com.uxforms.govukdesignsystem.dsl.{Section, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.build.GOVUKDesignSystemFormDefinitionBuildInfo
import com.uxforms.govukdesignsystem.form.page._

import scala.concurrent.{ExecutionContext, Future}

object Browse {

  val name = "browse"

  def apply[F <: Messages[F]]()
                             (implicit formLevelMessages: Future[Messages[F]], classLoader: ClassLoader, locale: Locale, templateRenderer: TemplateRenderer): Section[ResourceBundleMessages, F] = {

    val messages = ResourceBundleMessages.utf8("section/browse", classLoader, locale)
    val msgs = Future.successful(messages)

    section(
      name,
      msgs,
      withSectionCanContinue(SectionCanNeverContinue),

      Heading.xl("browseTitle", msgs),
      Paragraph.body("browseIntro", msgs),
      Heading.m("howToUseHeading", msgs),
      Paragraph.body("howToUse", msgs),
      Pre.pre("declareTheme", msgs, Pre.isCode),
      Paragraph.body("useTheme", msgs),
      Pre.pre("declareDsl", msgs, Pre.isCode ++
        applyNamedMessageValuesToMessageEntry(msgs, "declareDsl.pre.text", (_,_,_) => Map(
          "govukDesignSystemVersion" -> GOVUKDesignSystemFormDefinitionBuildInfo.govukDesignSystemVersion)
        )),

      SectionBreak.l("beforeTypography"),
      Heading.l("typography", msgs),
      List.list("typographyLinks", msgs, Seq(
        Link.toPage("toTypography", msgs, TypographyPage.urlPath),
        Link.toPage("toPre", msgs, PrePage.urlPath),
        Link.toPage("toSpacing", msgs, SpacingPage.urlPath)
      )),

      SectionBreak.l("beforeWidgets"),
      Heading.l("widgets", msgs),
      List.list("widgetLinks", msgs, Seq(
        Link.toPage("toBackLink", msgs, BackLinkPage.urlPath),
        Link.toPage("toButton", msgs, ButtonPage.urlPath),
        Link.toPage("toCharacterCount", msgs, CharacterCountPage.urlPath),
        Link.toPage("toCheckboxes", msgs, CheckboxesPage.urlPath),
        Link.toPage("toDate", msgs, DatePage.urlPath),
        Link.toPage("toDetails", msgs, DetailsPage.urlPath),
        Link.toPage("toFieldset", msgs, FieldsetPage.urlPath),
        Link.toPage("toFileUpload", msgs, FileUploadPage.urlPath),
        Link.toPage("toInsetText", msgs, InsetTextPage.urlPath),
        Link.toPage("toPanel", msgs, PanelPage.urlPath),
        Link.toPage("toRadios", msgs, RadiosPage.urlPath),
        Link.toPage("toSelect", msgs, SelectPage.urlPath),
        Link.toPage("toSummaryList", msgs, SummaryListPage.urlPath),
        Link.toPage("toTable", msgs, TablePage.urlPath),
        Link.toPage("toTabs", msgs, TabsPage.urlPath),
        Link.toPage("toTag", msgs, TagPage.urlPath),
        Link.toPage("toTextInput", msgs, InputPage.urlPath),
        Link.toPage("toTextArea", msgs, TextAreaPage.urlPath),
        Link.toPage("toWarningText", msgs, WarningTextPage.urlPath)
      )),

      SectionBreak.l("beforePatterns"),
      Heading.l("patterns", msgs),
      List.list("patternLinks", msgs, Seq(
        Link.toPage("toConfirmationPage", msgs, ConfirmationPage.urlPath),
        Link.toPage("toPageNotFoundPage", msgs, PageNotFoundPage.urlPath),
        Link.toPage("toProblemWithServicePage", msgs, ProblemWithServicePage.urlPath),
        Link.toPage("toQuestionPage", msgs, QuestionPage.urlPath),
        Link.toPage("toServiceUnavailablePage", msgs, ServiceUnavailablePage.urlPath),
        Link.toPage("toTaskListPage", msgs, TaskListPage.urlPath)
      ))
    )
  }

}

object SectionCanNeverContinue extends SectionCanContinue {
  override def given(formData: FormData)(implicit ec: ExecutionContext): Future[Boolean] =
    Future.successful(false)
}
