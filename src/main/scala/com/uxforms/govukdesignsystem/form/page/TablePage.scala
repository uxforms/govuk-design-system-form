package com.uxforms.govukdesignsystem.form.page

import com.uxforms.domain._
import com.uxforms.dsl.Form
import com.uxforms.dsl.helpers.All.withReceivers
import com.uxforms.dsl.helpers.ContainerHelper.withRedirectBeforeRender
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.styles.Spacing
import com.uxforms.govukdesignsystem.dsl.styles.Typography.size.m
import com.uxforms.govukdesignsystem.dsl.widget.Table.TableCell
import com.uxforms.govukdesignsystem.dsl.widget._
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.RequireInviteCodeToAccess
import play.api.libs.json.Json

import java.util.Locale
import scala.concurrent.{ExecutionContext, Future}

object TablePage {

  val urlPath = "table"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, executionContext: ExecutionContext, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/table", implicitly[ClassLoader], implicitly[Locale]))

    def buildHeader: (Form, ValidationErrors, RequestInfo) => Future[Seq[TableCell]] = (_, _, _) => {
      pageMessages.map(msgs => Seq(
        Map("text" -> msgs.getString("head.month").get),
        Map("text" -> msgs.getString("head.rate").get, "format" -> "numeric")
      ))
    }

    def buildRows: (Form, ValidationErrors, RequestInfo) => Future[(Boolean, Seq[Seq[TableCell]])] = (f, _, _) => {
      pageMessages.map(msgs => (true, Seq(
        Seq(
          Map("text" -> msgs.getString("jan").get),
          Map("text" -> ("£" + (f.data.textData \ "importedDataFromExternalAPI" \ "janRate").as[Int]), "format" -> "numeric")
        ),
        Seq(
          Map("text" -> msgs.getString("feb").get),
          Map("text" -> ("£" + (f.data.textData \ "importedDataFromExternalAPI" \ "febRate").as[Int]), "format" -> "numeric")
        ),
        Seq(
          Map("text" -> msgs.getString("mar").get),
          Map("text" -> ("£" + (f.data.textData \ "importedDataFromExternalAPI" \ "marRate").as[Int]), "format" -> "numeric")
        )
      )))
    }

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess) ++ withReceivers(Seq(insertTestData)),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("tableHeading", pageMessages),
      Paragraph.body("tableIntro", pageMessages),
      Paragraph.body("tableMessages", pageMessages),
      Pre.pre("tableCaption", pageMessages),
      Paragraph.body("tableWidget", pageMessages),
      Pre.pre("tableExample", pageMessages, Pre.isCode ++ Pre.classes(Spacing.responsiveMargin(9, Spacing.direction.bottom))),

      Table.table("exampleAmounts", pageMessages,
        Seq(
          Map("text" -> "Date"),
          Map("text" -> "Amount")
        ),
        Seq(
          Seq(
            Map("text" -> "First 6 weeks"),
            Map("text" -> "£109.80 per week")
          ),
          Seq(
            Map("text" -> "Next 33 weeks"),
            Map("text" -> "£109.80 per week")
          ),
          Seq(
            Map("text" -> "Total estimated pay"),
            Map("text" -> "£4,282.20")
          )
        ),
        Table.firstCellIsHeader ++ Table.captionClasses(Table.captionSize(m))
      ),

      Heading.m("tableDynamic", pageMessages, Heading.classes(Spacing.responsiveMargin(9, Spacing.direction.top))),
      Paragraph.body("tableDynamic", pageMessages),
      Paragraph.body("tableFormats", pageMessages),

      Paragraph.body("data", pageMessages),
      Pre.pre("data", pageMessages, Pre.isCode),
      Paragraph.body("dynamicMessages", pageMessages),
      Pre.pre("dynamicMessages", pageMessages),
      Paragraph.body("dynamicWidget", pageMessages),
      Pre.pre("dynamicWidget", pageMessages, Pre.isCode ++ Pre.classes(Spacing.responsiveMargin(9, Spacing.direction.bottom))),

      Table.dynamicTable("dynamicExample", pageMessages, buildHeader, buildRows, Table.captionClasses(Table.captionSize(m)))

    )

  }

  private val insertTestData = new DataTransformer {
    override def transform(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[DataTransformationResult] = {
      Future.successful(DataTransformationResult(form.data + Json.obj("importedDataFromExternalAPI" -> Json.obj(
        "janRate" -> 95,
        "febRate" -> 55,
        "marRate" -> 125
      )), None))
    }
  }


}
