package com.uxforms.govukdesignsystem.form.page

import java.util.Locale
import com.uxforms.domain._
import com.uxforms.dsl.Form
import com.uxforms.dsl.helpers.ContainerHelper.{withReceivers, withRedirectBeforeRender}
import com.uxforms.govukdesignsystem.dsl.Page.questionPage
import com.uxforms.govukdesignsystem.dsl.widget.{BackLink, Heading, Panel, Paragraph, Pre, SectionBreak}
import com.uxforms.govukdesignsystem.dsl.{Page, TemplateRenderer}
import com.uxforms.govukdesignsystem.form.page.support.RequireInviteCodeToAccess
import play.api.libs.json.Json
import com.uxforms.govukdesignsystem.dsl.message.MessageHelper._
import com.uxforms.govukdesignsystem.dsl.styles.Spacing
import com.uxforms.govukdesignsystem.dsl.styles.Spacing.direction

import scala.concurrent.{ExecutionContext, Future}

object PanelPage {

  val urlPath = "panel"

  def apply[F <: Messages[F]]()(implicit formLevelMessages: Future[Messages[F]], locale: Locale, classLoader: ClassLoader, renderer: TemplateRenderer): Page[ResourceBundleMessages, F] = {

    val pageMessages = Future.successful(ResourceBundleMessages.utf8("page/panel", implicitly[ClassLoader], implicitly[Locale]))
    val topMargin = Heading.classes(Spacing.responsiveMargin(9, direction.top))

    questionPage(urlPath, pageMessages,
      withRedirectBeforeRender(RequireInviteCodeToAccess) ++
        withReceivers(Seq(EnsureTestData)),

      Seq(
        BackLink.linkToLatestSection("backToLatestSection", pageMessages)
      ),

      Heading.xl("panelHeading", pageMessages),

      Paragraph.body("pan", pageMessages),
      Pre.pre("pan", pageMessages, Pre.isCode),
      Paragraph.body("panMsg", pageMessages),
      Pre.pre("panMsg", pageMessages, Pre.isCode),
      Panel.panel("pan", pageMessages),

      Heading.m("dyn", pageMessages, topMargin),
      Paragraph.body("dyn", pageMessages),
      Pre.pre("dyn", pageMessages),
      Paragraph.body("dynMsg", pageMessages),
      Pre.pre("dynMsg", pageMessages),
      Panel.panel("dynPan", pageMessages,
        applyNamedMessageValuesToMessageEntry(pageMessages, "dynPan.panel.html", (form: Form, _: ValidationErrors, _: RequestInfo) => {
          val reference = (form.data.textData \ "reference").asOpt[String].getOrElse("n/a")
          Map("reference" -> reference)
        })
      )
    )
  }
}

object EnsureTestData extends DataTransformer {
  override def transform(form: Form, requestInfo: RequestInfo)(implicit ec: ExecutionContext): Future[DataTransformationResult] = {
    Future.successful(DataTransformationResult(form.data + Json.obj("reference" -> "AABBCCDD"), None))
  }
}