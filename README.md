# govuk-design-system-form

A form, implemented in [UX Forms](https://uxforms.com), that demonstrates UX Forms' implementation of [GOV.UK's Design System](https://design-system.service.gov.uk).

The latest version is available on https://forms.uxforms.com/govuk-design-system-form.

This form acts as living documentation for how to use GOV.UK's Design System within UX Forms, and how each 
corresponding widget can be used and configured.

Note: The form's landing page is styled using UX Forms' house style - enter the provided passphrase to
progress to the form-proper. This is merely to prevent bots and webcrawlers from mistaking our demo for
a phishing site trying to pass itself off as a genuine government service.
