package com.uxforms.govukdesignsystem.form.page.support

import com.uxforms.domain.FormData
import com.uxforms.dsl.containers.{RedirectAfterSubmissions, RedirectTo, RedirectToPage}

import scala.concurrent.{ExecutionContext, Future}

object RedirectToPageAfterSubmission {
  def apply(urlPath: String): RedirectAfterSubmissions = {
    new RedirectAfterSubmissions {
      override def redirect(data: FormData)(implicit ec: ExecutionContext): Future[RedirectTo] = {
        Future.successful(RedirectToPage(urlPath, Map.empty, Map.empty))
      }
    }
  }
}
